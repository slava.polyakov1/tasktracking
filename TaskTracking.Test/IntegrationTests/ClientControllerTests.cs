﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using TaskTracking.Api.Models;
using TaskTracking.Business.Models.Account;
using TaskTracking.Business.Models.Workflow;
using TaskTracking.Data.EF;
using TaskTracking.Test.IntegrationTests.Fixture;
using TaskTracking.GlobalVariables;
using TaskTracking.Api.Models.User;

namespace TaskTracking.Test.IntegrationTests
{
    [TestFixture]
    public class ClientControllerTests
    {
        private TestFixture _factory;
        private const string RequestUri = "api/client/";
        private const string AuthUri = "api/account/login";
        private HttpClient _client;
        [SetUp]
        public void SetUp()
        {
            _factory = new TestFixture();
            _client = _factory.CreateClient();
        }
        [Test]
        public async System.Threading.Tasks.Task ClientController_CreateUserAndClient_AddsClientToDatabase()
        {
            string clientCreateUrl = RequestUri;
            string userCreateUrl = "api/account/registration";
            string userRoleAssignUrl = "api/admin/role/assign";

            RegisterDTO registerModel = new RegisterDTO { Email = "cornelius321@meta.ua", UserName = "TestUser", Password = "Abrakadabra123" };
            string jsonRegisterModel = JsonConvert.SerializeObject(registerModel);
            var userContent = new StringContent(jsonRegisterModel, Encoding.UTF8, "application/json");

            var authResponse = await _client.PostAsync(userCreateUrl, userContent);
            authResponse.EnsureSuccessStatusCode();

            using (var test = _factory.Services.CreateScope())
            {
                var context = test.ServiceProvider.GetService<ApplicationDbContext>();
                var user = await context.Users.FirstOrDefaultAsync(u => u.UserName == registerModel.UserName);
                UserRoleViewModel userRole = new UserRoleViewModel 
                { 
                    UserId = user.Id.ToString(), 
                    Roles = new List<string> { Constants.ClientRole, Constants.UserRole } 
                };
                string jsonUserRoleModel = JsonConvert.SerializeObject(userRole);
                var userRoleContent = new StringContent(jsonUserRoleModel, Encoding.UTF8, "application/json");

                await GetTokenRoleAdmin();
                await _client.PostAsync(userRoleAssignUrl, userRoleContent);

                ClientDTO clientModel = new ClientDTO { UserId = user.Id, CompanyName = "TestCompany" };
                var clientContent = new StringContent(JsonConvert.SerializeObject(clientModel), Encoding.UTF8, "application/json");

                var clientResponse = await _client.PostAsync(clientCreateUrl, clientContent);
                clientResponse.EnsureSuccessStatusCode();

                Assert.AreEqual(3, context.Clients.Count());
            }
        }
        [Test]
        public async System.Threading.Tasks.Task ClientController_GetAll_ReturnAll()
        {
            string url = RequestUri + "all";
            await GetTokenRoleAdmin();

            var clientResponse = await _client.GetAsync(url);

            clientResponse.EnsureSuccessStatusCode();
            string responseBody = await clientResponse.Content.ReadAsStringAsync();
            JObject jsonObj = JObject.Parse(responseBody);
            var clients = jsonObj["clients"].Value<JArray>().ToObject<IEnumerable<ClientDTO>>();
            Assert.AreEqual(2, clients.Count());
        }
        [Test]
        public async System.Threading.Tasks.Task ClientController_GetClient_ClientRole_ReturnSelfClient()
        {
            string url = RequestUri;
            await GetTokenRoleClient();

            var clientResponse = await _client.GetAsync(url);

            clientResponse.EnsureSuccessStatusCode();
            string responseBody = await clientResponse.Content.ReadAsStringAsync();
            JObject jsonObj = JObject.Parse(responseBody);
            var actual = jsonObj["data"].Value<JObject>().ToObject<ClientDTO>();
            string companyName = "Amazon";
            Assert.AreEqual(companyName, actual.CompanyName);
        }
        [Test]
        public async System.Threading.Tasks.Task ClientController_UpdateCompanyName_UpdatesFromDatabase()
        {
            int clientId = 1;
            string url = RequestUri + clientId;
            await GetTokenRoleAdmin();
            JObject jsonObj = new JObject();
            string expected = "Tesla";
            JProperty companyName = new JProperty("companyName", expected);
            jsonObj.Add(companyName);
            string jsonModel = JsonConvert.SerializeObject(jsonObj);
            var httpContent = new StringContent(jsonModel, System.Text.Encoding.UTF8, "application/json");

            var response = await _client.PutAsync(url, httpContent);
            response.EnsureSuccessStatusCode();

            using (var test = _factory.Services.CreateScope())
            {
                var context = test.ServiceProvider.GetService<ApplicationDbContext>();
                var actual = await context.Clients.FindAsync(clientId);

                Assert.AreEqual(expected, actual.CompanyName);
            }
        }
        [Test]
        public async System.Threading.Tasks.Task ClientController_Delete_DeletesFromDatabase()
        {
            int clientId = 1;
            string url = RequestUri + clientId;
            await GetTokenRoleAdmin();

            var response = await _client.DeleteAsync(url);
            response.EnsureSuccessStatusCode();

            using (var test = _factory.Services.CreateScope())
            {
                var context = test.ServiceProvider.GetService<ApplicationDbContext>();

                Assert.AreEqual(1, context.Clients.Count());
            }
        }
        #region Helper
        private async System.Threading.Tasks.Task GetTokenRoleClient()
        {
            await GetToken("Tony", "123qwe");
        }
        private async System.Threading.Tasks.Task GetTokenRoleManager()
        {
            await GetToken("James", "cvbnm");
        }
        private async System.Threading.Tasks.Task GetTokenRoleAdmin()
        {
            await GetToken("admin", "password");
        }
        private async System.Threading.Tasks.Task GetToken(string userName, string password)
        {
            JObject payload = new JObject();
            JProperty userNameProp = new JProperty("userName", userName);
            JProperty passwordProp = new JProperty("password", password);
            payload.Add(userNameProp);
            payload.Add(passwordProp);
            var httpContent = new StringContent(payload.ToString(), System.Text.Encoding.UTF8, "application/json");
            var authResponse = await _client.PostAsync(AuthUri, httpContent);
            authResponse.EnsureSuccessStatusCode();
            string responseBody = await authResponse.Content.ReadAsStringAsync();
            var jsonObj = JObject.Parse(responseBody);
            if (jsonObj != null)
            {
                string token = jsonObj["access_token"].Value<string>();
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            }
        }
        #endregion
    }
}
