﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using TaskTracking.Api;
using TaskTracking.Business.Models.Account;
using TaskTracking.Test.IntegrationTests.Fixture;

namespace TaskTracking.Test.IntegrationTests
{
    [TestFixture]
    public class AccountControllerTests
    {
        private TestFixture _factory;
        private const string RequestUri = "api/account/";
        private HttpClient _client;
        [SetUp]
        public void SetUp()
        {
            _factory = new TestFixture();
            _client = _factory.CreateClient();
        }
        [Test]
        [TestCase("admin", "password")]
        [TestCase("Tony", "123qwe")]
        [TestCase("Jake", "qwerty")]
        [TestCase("Wesley", "asdzxc")]
        [TestCase("Andy", "1q2w3e")]
        [TestCase("James", "cvbnm")]
        public async System.Threading.Tasks.Task AccountController_Login_ReturnToken(string userName, string password)
        {
            string url = RequestUri + "login";
            LoginDTO loginModel = new LoginDTO { UserName = userName, Password = password };
            string jsonModel = JsonConvert.SerializeObject(loginModel);
            var httpContent = new StringContent(jsonModel, System.Text.Encoding.UTF8, "application/json");

            var authResponse = await _client.PostAsync(url, httpContent);

            authResponse.EnsureSuccessStatusCode();
            string responseBody = await authResponse.Content.ReadAsStringAsync();
            var jsonObj = JObject.Parse(responseBody);
            string token = jsonObj["access_token"].Value<string>();
            Assert.True(token.Length > 0);
        }
        [Test]
        [TestCase("cornelius321@meta.ua", "Viktor", "Wolverine321")]
        public async System.Threading.Tasks.Task AccountController_Register_ReturnStatusCode201(string email, string userName, string password)
        {
            string url = RequestUri + "registration";
            RegisterDTO registerModel = new RegisterDTO { Email = email, UserName = userName, Password = password };
            string jsonModel = JsonConvert.SerializeObject(registerModel);
            var httpContent = new StringContent(jsonModel, System.Text.Encoding.UTF8, "application/json");

            var authResponse = await _client.PostAsync(url, httpContent);

            authResponse.EnsureSuccessStatusCode();
            var actual = authResponse.StatusCode;
            Assert.AreEqual(System.Net.HttpStatusCode.Created, actual);
        }
        [Test]
        [TestCase("cornelius321@meta.ua", "Viktor", "123qwe")]
        public async System.Threading.Tasks.Task AccountController_Register_ReturnStatusCode400(string email, string userName, string password)
        {
            string url = RequestUri + "registration";
            RegisterDTO registerModel = new RegisterDTO { Email = email, UserName = userName, Password = password };
            string jsonModel = JsonConvert.SerializeObject(registerModel);
            var httpContent = new StringContent(jsonModel, System.Text.Encoding.UTF8, "application/json");

            var authResponse = await _client.PostAsync(url, httpContent);

            var actual = authResponse.StatusCode;
            Assert.AreEqual(System.Net.HttpStatusCode.BadRequest, actual);
        }

        [TearDown]
        public void TearDown()
        {
            _factory.Dispose();
            _client.Dispose();
        }
    }
}
