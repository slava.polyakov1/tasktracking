﻿using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using TaskTracking.Business.Models.Workflow;
using TaskTracking.Test.IntegrationTests.Fixture;

namespace TaskTracking.Test.IntegrationTests
{
    [TestFixture]
    public class ProjectControllerTests
    {
        private TestFixture _factory;
        private const string RequestUri = "api/project/";
        private const string AuthUri = "api/account/login";
        private HttpClient _client;
        [SetUp]
        public void SetUp()
        {
            _factory = new TestFixture();
            _client = _factory.CreateClient();
        }
        [Test]
        public async System.Threading.Tasks.Task ProjectController_GetAll_ReturnAllProjects()
        {
            string url = RequestUri + "all";
            await GetTokenRoleAdmin();

            var response = await _client.GetAsync(url);

            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();
            JObject jsonObj = JObject.Parse(responseBody);
            var projects = jsonObj["data"].Value<JArray>().ToObject<IEnumerable<ProjectDTO>>();
            Assert.AreEqual(2, projects.Count());
        }
        #region Helper
        private async System.Threading.Tasks.Task GetTokenRoleClient()
        {
            await GetToken("Tony", "123qwe");
        }
        private async System.Threading.Tasks.Task GetTokenRoleManager()
        {
            await GetToken("James", "cvbnm");
        }
        private async System.Threading.Tasks.Task GetTokenRoleAdmin()
        {
            await GetToken("admin", "password");
        }
        private async System.Threading.Tasks.Task GetToken(string userName, string password)
        {
            JObject payload = new JObject();
            JProperty userNameProp = new JProperty("userName", userName);
            JProperty passwordProp = new JProperty("password", password);
            payload.Add(userNameProp);
            payload.Add(passwordProp);
            var httpContent = new StringContent(payload.ToString(), System.Text.Encoding.UTF8, "application/json");
            var authResponse = await _client.PostAsync(AuthUri, httpContent);
            authResponse.EnsureSuccessStatusCode();
            string responseBody = await authResponse.Content.ReadAsStringAsync();
            var jsonObj = JObject.Parse(responseBody);
            if (jsonObj != null)
            {
                string token = jsonObj["access_token"].Value<string>();
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            }
        }
        #endregion
    }
}
