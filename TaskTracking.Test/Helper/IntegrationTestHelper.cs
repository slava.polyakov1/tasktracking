﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using TaskTracking.Business.Mapper;

namespace TaskTracking.Test.Helper
{
    public class IntegrationTestHelper
    {
        public static Mapper CreateMapperProfile()
        {
            var myProfile = new AutoMapperProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));

            return new Mapper(configuration);
        }
    }
}
