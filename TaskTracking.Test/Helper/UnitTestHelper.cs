﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TaskTracking.Data.EF;
using TaskTracking.Data.Entities.Identity;
using TaskTracking.Data.Entities.Workflow;
using TaskTracking.Data.Identity;
using TaskTracking.GlobalVariables;
using System.Linq;

namespace TaskTracking.Test.Helper
{
    public class UnitTestHelper
    {
        private static UserManager<ApplicationUser> _userManager;
        public static DbContextOptions<ApplicationDbContext> GetUnitTestDbOptions()
        {

            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            using (var context = new ApplicationDbContext(options))
            {
                //SeedData(context);
            }
            return options;
        }

        public static async void SeedData(ApplicationDbContext context, UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager)
        {
            _userManager = userManager;

            var adminRoleId = Guid.NewGuid();
            var managerRoleId = Guid.NewGuid();
            var clientRoleId = Guid.NewGuid();
            var employeeRoleId = Guid.NewGuid();
            var userRoleId = Guid.NewGuid();

            var adminRole = new ApplicationRole { Id = adminRoleId, Name = Constants.AdminRole };
            var managerRole = new ApplicationRole { Id = managerRoleId, Name = Constants.ManagerRole };
            var clientRole = new ApplicationRole { Id = clientRoleId, Name = Constants.ClientRole };
            var employeeRole = new ApplicationRole { Id = employeeRoleId, Name = Constants.EmployeeRole };
            var userRole = new ApplicationRole { Id = userRoleId, Name = Constants.UserRole };

            var admin = new ApplicationUser { Id = Guid.NewGuid(), UserName = "admin", Email = "admin@mail.com", EmailConfirmed = true, SecurityStamp = new Guid().ToString("D"), NormalizedEmail = "ADMIN@MAIL.COM", NormalizedUserName = "ADMIN" };
            var user1 = new ApplicationUser { Id = Guid.NewGuid(), UserName = "Tony", Email = "user1@mail.com", EmailConfirmed = true };
            var user2 = new ApplicationUser { Id = Guid.NewGuid(), UserName = "Jake", Email = "user2@mail.com", EmailConfirmed = false };
            var user3 = new ApplicationUser { Id = Guid.NewGuid(), UserName = "Wesley", Email = "user3@mail.com", EmailConfirmed = false };
            var user4 = new ApplicationUser { Id = Guid.NewGuid(), UserName = "Andy", Email = "user4@mail.com", EmailConfirmed = false };
            var user5 = new ApplicationUser { Id = Guid.NewGuid(), UserName = "James", Email = "user5@mail.com", EmailConfirmed = false };
            var client1 = new Client { Id = 1, UserId = user1.Id, CompanyName = "Amazon" };
            var client2 = new Client { Id = 2, UserId = user2.Id, CompanyName = "Spacelink" };
            var employee1 = new Employee { Id = 1, UserId = user3.Id };
            var employee2 = new Employee { Id = 2, UserId = user4.Id };
            var project1 = new Project { Id = 1, Title = "Initialize bd", ClientId = 1 };
            var project2 = new Project { Id = 2, Title = "Missile launch", ClientId = 2 };
            admin.PasswordHash = FakePassGenerate(admin, "password");
            user1.PasswordHash = FakePassGenerate(user1, "123qwe");
            user2.PasswordHash = FakePassGenerate(user2, "qwerty");
            user3.PasswordHash = FakePassGenerate(user3, "asdzxc");
            user4.PasswordHash = FakePassGenerate(user4, "1q2w3e");
            user5.PasswordHash = FakePassGenerate(user5, "cvbnm");

            context.Clients.Add(client1);
            context.Clients.Add(client2);
            context.Employees.Add(employee1);
            context.Employees.Add(employee2);
            context.Projects.Add(project1);
            context.Projects.Add(project2);
            context.SaveChanges();

            var tasksRoleArr = new System.Threading.Tasks.Task[]
            {
                roleManager.CreateAsync(adminRole),
                roleManager.CreateAsync(managerRole),
                roleManager.CreateAsync(clientRole),
                roleManager.CreateAsync(employeeRole),
                roleManager.CreateAsync(userRole)
            };
            await System.Threading.Tasks.Task.WhenAll(tasksRoleArr);
            var tasksAssignRole = new System.Threading.Tasks.Task[]
            {
                CreateUserAndAssignRoleAsync(admin, Constants.AdminRole),
                CreateUserAndAssignRoleAsync(user1, new string[] { Constants.UserRole, Constants.ClientRole }),
                CreateUserAndAssignRoleAsync(user2, new string[] { Constants.UserRole, Constants.ClientRole }),
                CreateUserAndAssignRoleAsync(user3, new string[] { Constants.UserRole, Constants.EmployeeRole }),
                CreateUserAndAssignRoleAsync(user4, new string[] { Constants.UserRole, Constants.EmployeeRole }),
                CreateUserAndAssignRoleAsync(user5, new string[] { Constants.UserRole, Constants.ManagerRole })
            };
            await System.Threading.Tasks.Task.WhenAll(tasksAssignRole);
        }
        private static async System.Threading.Tasks.Task CreateUserAndAssignRoleAsync(ApplicationUser user, params string[] roleName)
        {
            await _userManager.CreateAsync(user);
            await _userManager.AddToRolesAsync(user, roleName);
        }
        private static string FakePassGenerate(ApplicationUser user, string password)
        {
            var passHash = new PasswordHasher<ApplicationUser>();
            return passHash.HashPassword(user, password);
        }
    }
}
