import Vue from 'vue'
import App from './App.vue'
import store from './store';
import axios from 'axios';
import router from './router';
import vuetify from './plugins/vuetify';
import "normalize.css";
//import VueCookies from 'vue-cookies';


Vue.config.productionTip = true;

Vue.prototype.$axios = axios;

const token = localStorage.getItem('token')
if (token) {
    Vue.prototype.$axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
} else {
    Vue.prototype.$axios.defaults.headers.common['Authorization'] = ''
}

new Vue({
    render: h => h(App),
    store,
    vuetify,
    axios,
    router
}).$mount('#app')


export default axios.create({
    baseURL: store.state.apiDomain,
    headers: {
        'Content-Type': 'application/json',
        'Authorization': token
    }
})