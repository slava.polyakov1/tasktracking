﻿import axios from 'axios'

export default {
    namespaced: true,
    state: {
        clientAllData: [],
        clientData: []
    },
    getters: {
        getClientAllData: state => state.clientAllData,
        getClientData: state => state.clientData
    },
    mutations: {
        setClientAllData(state, payload) {
            state.clientAllData = payload;
        },
        setClientData(state, payload) {
            state.clientData = payload;
        }
    },
    actions: {
        onClientAllData({ commit, rootState }) {
            return new Promise((resolve, reject) => {
                axios({
                    url:
                        rootState.apiDomain +
                        rootState.apiUrls.workflow.clientAll,
                    method: 'GET'
                })
                    .then(resp => {
                        commit('setClientAllData', resp.data.clients)
                        resolve(resp)
                    })
                    .catch(err => {
                        reject(err)
                    })
            })
        },
        onClientData({ commit, rootState }) {
            return new Promise((resolve, reject) => {
                axios({
                    url:
                        rootState.apiDomain +
                        rootState.apiUrls.workflow.clientRoot,
                    method: 'GET'
                })
                    .then(resp => {
                        commit('setClientData', resp.data.data)
                        resolve(resp)
                    })
                    .catch(err => {
                        reject(err)
                    })
            })
        },
        clientCreate({ rootState }, client) {
            return new Promise((resolve, reject) => {
                axios({
                    url:
                        rootState.apiDomain +
                        rootState.apiUrls.workflow.clientRoot,
                    data: client,
                    method: 'POST'
                })
                    .then(resp => {
                        resolve(resp)
                    })
                    .catch(err => {
                        reject(err)
                    })
            })
        },
        clientEdit({ rootState }, client) {
            return new Promise((resolve, reject) => {
                axios({
                    url:
                        rootState.apiDomain +
                        rootState.apiUrls.workflow.clientRoot,
                    data: client,
                    method: 'PUT'
                })
                    .then(resp => {
                        resolve(resp)
                    })
                    .catch(err => {
                        reject(err)
                    })
            })
        }
    }
}