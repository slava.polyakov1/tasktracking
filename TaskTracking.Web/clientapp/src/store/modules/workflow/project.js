﻿import axios from 'axios'

export default {
    namespaced: true,
    state: {
        projectAllData: [],
        projectData: []
    },
    getters: {
        getProjectAllData: state => state.projectAllData,
        getProjectData: state => state.projectData
    },
    mutations: {
        setProjectAllData(state, payload) {
            state.projectAllData = payload;
        },
        setProjectData(state, payload) {
            state.projectData = payload;
        }
    },
    actions: {
        onProjectAllData({ commit, rootState }) {
            return new Promise((resolve, reject) => {
                axios({
                    url:
                        rootState.apiDomain +
                        rootState.apiUrls.workflow.projectAll,
                    method: 'GET'
                })
                    .then(resp => {
                        commit('setProjectAllData', resp.data.data)
                        resolve(resp)
                    })
                    .catch(err => {
                        reject(err)
                    })
            })
        },
        onProjectAllWithDetailsData({ commit, rootState }) {
            return new Promise((resolve, reject) => {
                axios({
                    url:
                        rootState.apiDomain +
                        rootState.apiUrls.workflow.project.details,
                    method: 'GET'
                })
                    .then(resp => {
                        commit('setProjectAllData', resp.data.data)
                        resolve(resp)
                    })
                    .catch(err => {
                        reject(err)
                    })
            })
        },
        onProjectData({ commit, rootState }) {
            return new Promise((resolve, reject) => {
                axios({
                    url:
                        rootState.apiDomain +
                        rootState.apiUrls.workflow.project,
                    method: 'GET'
                })
                    .then(resp => {
                        commit('setProjectData', resp.data.data)
                        resolve(resp)
                    })
                    .catch(err => {
                        reject(err)
                    })
            })
        },
        projectCreate({ rootState }, project) {
            return new Promise((resolve, reject) => {
                axios({
                    url:
                        rootState.apiDomain +
                        rootState.apiUrls.workflow.project.edit,
                    data: project,
                    method: 'PUT'
                })
                    .then(resp => {
                        resolve(resp)
                    })
                    .catch(err => {
                        reject(err)
                    })
            })
        }
    }
}