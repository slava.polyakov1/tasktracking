﻿import axios from 'axios'

export default {
    namespaced: true,
    state: {
        taskAllData: [],
        taskData: [],
    },
    getters: {
        getTaskAllData: state => state.taskAllData,
        getTaskData: state => state.taskData
    },
    mutations: {
        setTaskAllData(state, payload) {
            state.taskAllData = payload;
        },
        setTaskData(state, payload) {
            state.taskData = payload;
        }
    },
    actions: {
        onTaskAllData({ commit, rootState }) {
            return new Promise((resolve, reject) => {
                axios({
                    url:
                        rootState.apiDomain +
                        rootState.apiUrls.workflow.taskAll,
                    method: 'GET'
                })
                    .then(resp => {
                        commit('setTaskAllData', resp.data.data)
                        resolve(resp)
                    })
                    .catch(err => {
                        reject(err)
                    })
            })
        },
        onTaskData({ commit, rootState }) {
            return new Promise((resolve, reject) => {
                axios({
                    url:
                        rootState.apiDomain +
                        rootState.apiUrls.workflow.taskRoot,
                    method: 'GET'
                })
                    .then(resp => {
                        commit('setTaskData', resp.data.data)
                        resolve(resp)
                    })
                    .catch(err => {
                        reject(err)
                    })
            })
        },
        taskCreate({ rootState }, task) {
            return new Promise((resolve, reject) => {
                axios({
                    url:
                        rootState.apiDomain +
                        rootState.apiUrls.workflow.taskRoot,
                    data: task,
                    method: 'POST'
                })
                    .then(resp => {
                        resolve(resp)
                    })
                    .catch(err => {
                        reject(err)
                    })
            })
        }
    }
}