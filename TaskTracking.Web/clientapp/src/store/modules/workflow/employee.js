﻿import axios from 'axios'

export default {
    namespaced: true,
    state: {
        employeeAllData: [],
        employeeData: []
    },
    getters: {
        getEmployeeAllData: state => state.employeeAllData,
        getEmployeeData: state => state.employeeData
    },
    mutations: {
        setEmployeeAllData(state, payload) {
            state.employeeAllData = payload;
        },
        setEmployeeData(state, payload) {
            state.employeeData = payload;
        }
    },
    actions: {
        onEmployeeAllData({ commit, rootState }) {
            return new Promise((resolve, reject) => {
                axios({
                    url:
                        rootState.apiDomain +
                        rootState.apiUrls.workflow.employeeAll,
                    method: 'GET'
                })
                    .then(resp => {
                        commit('setEmployeeAllData', resp.data.data)
                        resolve(resp)
                    })
                    .catch(err => {
                        reject(err)
                    })
            })
        },
        onEmployeeData({ commit, rootState }) {
            return new Promise((resolve, reject) => {
                axios({
                    url:
                        rootState.apiDomain +
                        rootState.apiUrls.workflow.employeeRoot,
                    method: 'GET'
                })
                    .then(resp => {
                        commit('setEmployeeData', resp.data.data)
                        resolve(resp)
                    })
                    .catch(err => {
                        reject(err)
                    })
            })
        },
        employeeCreate({ rootState }, employee) {
            return new Promise((resolve, reject) => {
                axios({
                    url:
                        rootState.apiDomain +
                        rootState.apiUrls.workflow.employeeRoot,
                    data: employee,
                    method: 'POST'
                })
                    .then(resp => {
                        resolve(resp)
                    })
                    .catch(err => {
                        reject(err)
                    })
            })
        },
    }
}