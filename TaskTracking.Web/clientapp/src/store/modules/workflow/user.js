﻿import axios from 'axios'

export default {
    namespaced: true,
    state: {
        userData: [],
        userPersonalData: [],
        roles: [],
        rolesAll: [],
        usersWithRoles: []
    },
    getters: {
        getUserData: state => state.userData,
        getUserPersonalData: state => state.userPersonalData,
        getRoles: state => state.roles,
        getRolesAll: state => state.rolesAll,
        getUsersWithRoles: state => state.usersWithRoles,
    },
    mutations: {
        setUserData(state, payload) {
            state.userData = payload;
        },
        setUserPersonalData(state, payload) {
            state.userPersonalData = payload;
        },
        setRoles(state, payload) {
            state.roles = payload;
        },
        setRolesAll(state, payload) {
            state.rolesAll = payload;
        },
        setUsersWithRoles(state, payload) {
            state.usersWithRoles = payload;
        }
    },
    actions: {
        onUserData({ commit, rootState }) {
            return new Promise((resolve, reject) => {
                axios({
                    url:
                        rootState.apiDomain +
                        rootState.apiUrls.user.usersAll,
                    method: 'GET'
                })
                    .then(resp => {
                        commit('setUserData', resp.data.data)
                        resolve(resp)
                    })
                    .catch(err => {
                        reject(err)
                    })
            })
        },
        onUserPersonalData({ commit, rootState }) {
            return new Promise((resolve, reject) => {
                axios({
                    url: rootState.apiDomain + rootState.apiUrls.user.userPersonal,
                    method: 'GET'
                })
                    .then(resp => {
                        const user = resp.data.data
                        commit('setUserPersonalData', user)
                        resolve(resp)
                    })
                    .catch(err => {
                        reject(err)
                    })
            })
        },
        onUserRoles({ commit, rootState }) {
            return new Promise((resolve, reject) => {
                axios({
                    url: rootState.apiDomain + rootState.apiUrls.user.roles,
                    method: 'GET'
                })
                    .then(resp => {
                        const roles = resp.data.roles
                        localStorage.setItem('roles', roles)
                        commit('setRoles', roles)
                        resolve(resp)
                    })
                    .catch(err => {
                        reject(err)
                    })
            })
        },
        onRolesAll({ commit, rootState }) {
            return new Promise((resolve, reject) => {
                axios({
                    url: rootState.apiDomain + rootState.apiUrls.user.rolesAll,
                    method: 'GET'
                })
                    .then(resp => {
                        let roles = resp.data.roles;
                        commit('setRolesAll', roles);
                        resolve(resp);
                    })
                    .catch(err => {
                        reject(err)
                    })
            })
        },
        onUsersWithRoles({ commit, rootState }) {
            return new Promise((resolve, reject) => {
                axios({
                    url: rootState.apiDomain + rootState.apiUrls.admin.userWithRoles,
                    method: 'GET'
                })
                    .then(resp => {
                        let usersWithRoles = resp.data.data;
                        commit('setUsersWithRoles', usersWithRoles);
                        resolve(resp);
                    })
                    .catch(err => {
                        reject(err)
                    })
            })
        },
        roleAssign({ rootState }, role) {
            return new Promise((resolve, reject) => {
                axios({
                    url: rootState.apiDomain + rootState.apiUrls.admin.roleAssign,
                    data: role,
                    method: 'POST'
                })
                    .then(resp => {
                        resolve(resp);
                    })
                    .catch(err => {
                        reject(err)
                    })
            })
        }
    }
}