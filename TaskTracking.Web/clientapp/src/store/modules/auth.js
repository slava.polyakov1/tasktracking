﻿import axios from 'axios'
//import cookies from 'vue-cookies'

export default {
    namespaced: true,
    state: {
        status: '',
        token: localStorage.getItem('token')
        //next_step: ''
    },
    mutations: {
        auth_request(state) {
            state.status = 'loading'
        },
        auth_success(state, token) {
            state.status = 'success'
            state.token = token
            state.next_step = ''
        },
        auth_error(state) {
            state.status = 'error'
        },
        logout(state) {
            state.status = ''
            state.token = ''
        }
    },
    getters: {
        isLoggedIn: state => !!state.token,
        authStatus: state => state.status,
        authToken: state => state.token,
        nextStep: state => state.next_step
    },
    actions: {
        login({ commit, rootState }, user) {
            return new Promise((resolve, reject) => {
                commit('auth_request')
                axios({
                    url: rootState.apiDomain + rootState.apiUrls.auth.login,
                    data: user,
                    method: 'POST'
                })
                    .then(resp => {
                        const token = resp.data.access_token
                        axios.defaults.headers.common['Authorization'] =
                            'Bearer ' + token
                        localStorage.setItem('token', token)
                        commit('auth_success', token)
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error')
                        //cookies.remove('token')
                        reject(err)
                    })
            })
        },
        logout({ commit }) {
            return new Promise(resolve => {
                commit('logout')
                //cookies.remove('token')
                localStorage.removeItem("token")
                localStorage.removeItem("roles")
                //localStorage.removeItem("NoFirstLogin")
                delete axios.defaults.headers.common['Authorization']
                resolve()
            })
        },
        register({ rootState }, user) {
            return new Promise((resolve, reject) => {
                // commit('auth_request')
                axios({
                    url: rootState.apiDomain + rootState.apiUrls.auth.registration,
                    data: user,
                    method: 'POST'
                })
                    .then(resp => {
                        resolve(resp)
                    })
                    .catch(err => {
                        reject(err)
                    })
            })
        }
    }
}
