﻿import Vue from 'vue';
import Vuex from 'vuex';


import auth from './modules/auth'
import task from './modules/workflow/task'
import project from './modules/workflow/project'
import user from './modules/workflow/user'
import employee from './modules/workflow/employee'
import client from './modules/workflow/client'

Vue.use(Vuex)

const store = new Vuex.Store({
    modules: {
        auth,
        task,
        project,
        user,
        employee,
        client
    },
    namespaced: true,
    state: {
        apiDomain: 'https://localhost:44312',
        apiUrls: {
            admin: {
                roleAssign: '/api/admin/role/assign',
                userWithRoles: '/api/admin/users/roles',
            },
            auth: {
                login: '/api/account/login',
                registration: '/api/account/registration'
            },
            user: {
                userPersonal: '/api/user',
                usersAll: '/api/user/all',
                roles: '/api/user/roles',
                rolesAll: '/api/user/roles/all'
            },
            workflow: {
                taskAll: '/api/task/all',
                projectAll: '/api/project/all',
                employeeAll: '/api/employee/all',
                clientAll: '/api/client/all',
                employeeRoot: '/api/employee',
                clientRoot: '/api/client',
                projectRoot: '/api/project',
                taskRoot: '/api/task',
                project: {
                    details: '/api/project/all/details',
                    edit: '/api/project/edit'
                }
            }
        }
    },
    getters: {
        apiDomain: state => state.apiDomain
    }
})
export default store