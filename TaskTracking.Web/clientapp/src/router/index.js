﻿import Vue from 'vue'
import VueRouter from 'vue-router'
import Index from '../views/Index.vue'
import Login from '../views/Login.vue'
import Roles from '../views/user/Roles.vue'
import Task from '../views/task/Task.vue'
import TaskEdit from '../views/task/TaskEdit.vue'
import Profile from '../views/user/Profile.vue'
import Project from '../views/project/Project.vue'

Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,

    routes: [
        {
            path: '/',
            name: 'Home',
            component: Index,
            meta: {
                auth: true
            }
        },
        {
            path: '/login',
            name: 'Login',
            component: Login,
            meta: {
                auth: false
            }
        },
        {
            path: '/profile',
            name: 'Profile',
            component: Profile,
            meta: {
                auth: true
            }
        },
        {
            path: '/roles',
            name: 'Roles',
            component: Roles,
            meta: {
                auth: true
            }
        },
        {
            path: '/project',
            name: 'Project',
            component: Project,
            meta: {
                auth: true
            }
        },
        {
            path: '/task',
            name: 'Task',
            component: Task,
            meta: {
                auth: true
            }
        },
        {
            path: '/task/:id',
            name: 'TaskEdit',
            component: TaskEdit,
            meta: {
                auth: true
            }
        }
    ]
})
export default router