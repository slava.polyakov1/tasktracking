﻿const fs = require('fs')
const path = require('path')
module.exports = {
    devServer: {
        host: 'localhost',
        port: '49012'
    },
    transpileDependencies: ['vuetify']
}
