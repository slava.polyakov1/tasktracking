﻿using System;
using System.Linq;

namespace TaskTracking.Data.Interfaces.Workflow
{
    public interface ITaskRepository : IRepository<TaskTracking.Data.Entities.Workflow.Task>
    {
        IQueryable<Entities.Workflow.Task> FindTaskByFilter(Func<Entities.Workflow.Task, bool> predicate);
    }
}
