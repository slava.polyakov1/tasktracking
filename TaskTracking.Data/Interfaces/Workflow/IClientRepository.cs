﻿using System;
using TaskTracking.Data.Entities.Workflow;

namespace TaskTracking.Data.Interfaces.Workflow
{
    public interface IClientRepository : IRepository<Client>
    {
        System.Threading.Tasks.Task<Client> GetByUserIdAsync(Guid userId);
    }
}
