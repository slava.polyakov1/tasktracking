﻿using System;
using System.Linq;
using TaskTracking.Data.Entities.Workflow;

namespace TaskTracking.Data.Interfaces.Workflow
{
    public interface IProjectRepository : IRepository<Project>
    {
        IQueryable<Project> FindProjectByFilter(Func<Project, bool> predicate);
        IQueryable<Project> FindAllWithDetails();
    }
}
