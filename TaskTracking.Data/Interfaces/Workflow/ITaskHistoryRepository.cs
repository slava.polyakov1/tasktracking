﻿using TaskTracking.Data.Entities.Workflow;

namespace TaskTracking.Data.Interfaces.Workflow
{
    public interface ITaskHistoryRepository : IRepository<TaskHistory>
    {

    }
}
