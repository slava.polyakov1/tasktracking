﻿using System;
using TaskTracking.Data.Entities.Workflow;

namespace TaskTracking.Data.Interfaces.Workflow
{
    public interface IEmployeeRepository : IRepository<Employee>
    {
        System.Threading.Tasks.Task<Employee> GetByUserIdAsync(Guid userId);
    }
}
