﻿using System.Linq;
using System.Threading.Tasks;
using TaskTracking.Data.Entities;

namespace TaskTracking.Data.Interfaces
{
    public interface IRepository<TEntity> where TEntity : BaseEntity
    {
        IQueryable<TEntity> FindAll();
        Task<TEntity> GetByIdAsync(int id);
        Task AddAsync(TEntity entity);
        void Update(TEntity entity);
        /// <summary>
        /// Task won't be deleted, but will be closed
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task DeleteByIdAsync(int id);
    }
}
