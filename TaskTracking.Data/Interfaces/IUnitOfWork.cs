﻿using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Threading.Tasks;
using TaskTracking.Data.Interfaces.Workflow;

namespace TaskTracking.Data.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        //ApplicationSignInManager SignInManager { get; }
        //ApplicationUserManager UserManager { get; }
        //ApplicationRoleManager RoleManager { get; }
        IClientRepository ClientRepository { get; }
        IEmployeeRepository EmployeeRepository { get; }
        IProjectRepository ProjectRepository { get; }
        ITaskRepository TaskRepository { get; }
        ITaskHistoryRepository TaskHistoryRepository { get; }
        Task<int> SaveAsync();
        Task<IDbContextTransaction> BeginTransactionAsync();
    }
}
