﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using TaskTracking.Data.EF;
using TaskTracking.Data.Entities.Workflow;
using TaskTracking.Data.Interfaces.Workflow;

namespace TaskTracking.Data.Repositories.Workflow
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private readonly ApplicationDbContext db;
        public EmployeeRepository(ApplicationDbContext context)
        {
            db = context;
        }
        public async System.Threading.Tasks.Task AddAsync(Employee entity)
        {
            await db.Employees.AddAsync(entity);
            await db.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task DeleteByIdAsync(int id)
        {
            Employee employee = await db.Employees.FindAsync(id);
            if (employee != null)
                db.Employees.Remove(employee);
            await db.SaveChangesAsync();
        }

        public IQueryable<Employee> FindAll()
        {
            return db.Employees.Include(e => e.User).AsNoTracking().AsQueryable();
        }
        public async Task<Employee> GetByUserIdAsync(Guid userId)
        {
            return await db.Employees.AsNoTracking().FirstOrDefaultAsync(e => e.UserId == userId);
        }
        public async Task<Employee> GetByIdAsync(int id)
        {
            return await db.Employees.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
        }

        public void Update(Employee entity)
        {
            db.Employees.Update(entity);
            db.SaveChanges();
        }
    }
}
