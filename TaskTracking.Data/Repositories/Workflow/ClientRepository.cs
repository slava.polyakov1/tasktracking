﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using TaskTracking.Data.EF;
using TaskTracking.Data.Entities.Workflow;
using TaskTracking.Data.Interfaces.Workflow;

namespace TaskTracking.Data.Repositories.Workflow
{
    /// <summary>
    /// Client repository <see cref="IClientRepository"/>
    /// </summary>
    public class ClientRepository : IClientRepository
    {
        private readonly ApplicationDbContext db;
        public ClientRepository(ApplicationDbContext context)
        {
            db = context;
        }
        public async System.Threading.Tasks.Task AddAsync(Client entity)
        {
            await db.Clients.AddAsync(entity);
            await db.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task DeleteByIdAsync(int id)
        {
            Client client = await db.Clients.FindAsync(id);
            if (client != null)
                db.Clients.Remove(client);
            await db.SaveChangesAsync();
        }

        public IQueryable<Client> FindAll()
        {
            return db.Clients.AsNoTracking().AsQueryable();
        }
        public async Task<Client> GetByUserIdAsync(Guid userId)
        {
            return await db.Clients.AsNoTracking().FirstOrDefaultAsync(c => c.UserId == userId);
        }
        public async Task<Client> GetByIdAsync(int id)
        {
            return await db.Clients.AsNoTracking().FirstOrDefaultAsync(c => c.Id == id);
        }

        public void Update(Client entity)
        {
            db.Clients.Update(entity);
            db.SaveChanges();
        }
    }
}
