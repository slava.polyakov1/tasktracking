﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using TaskTracking.Data.EF;
using TaskTracking.Data.Entities.Workflow;
using TaskTracking.Data.Interfaces.Workflow;

namespace TaskTracking.Data.Repositories.Workflow
{
    public class TaskHistoryRepository : ITaskHistoryRepository
    {
        private readonly ApplicationDbContext db;
        public TaskHistoryRepository(ApplicationDbContext context)
        {
            db = context;
        }
        public async System.Threading.Tasks.Task AddAsync(TaskHistory entity)
        {
            await db.TaskHistories.AddAsync(entity);
            await db.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task DeleteByIdAsync(int id)
        {
            TaskHistory taskHistory = await db.TaskHistories.FindAsync(id);
            if (taskHistory != null)
                db.TaskHistories.Remove(taskHistory);
            await db.SaveChangesAsync();
        }

        public IQueryable<TaskHistory> FindAll()
        {
            return db.TaskHistories.AsNoTracking().AsQueryable();
        }

        public async Task<TaskHistory> GetByIdAsync(int id)
        {
            return await db.TaskHistories.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
        }

        public void Update(TaskHistory entity)
        {
            db.TaskHistories.Update(entity);
            db.SaveChanges();
        }
    }
}
