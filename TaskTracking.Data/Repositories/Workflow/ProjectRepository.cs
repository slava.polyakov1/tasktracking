﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using TaskTracking.Data.EF;
using TaskTracking.Data.Entities.Workflow;
using TaskTracking.Data.Interfaces.Workflow;

namespace TaskTracking.Data.Repositories.Workflow
{
    public class ProjectRepository : IProjectRepository
    {
        private readonly ApplicationDbContext db;
        public ProjectRepository(ApplicationDbContext context)
        {
            db = context;
        }
        public async System.Threading.Tasks.Task AddAsync(Project entity)
        {
            await db.Projects.AddAsync(entity);
            await db.SaveChangesAsync();
        }
        public async System.Threading.Tasks.Task DeleteByIdAsync(int id)
        {
            Project project = await db.Projects.FindAsync(id);
            if (project != null)
                db.Projects.Remove(project);
            await db.SaveChangesAsync();
        }
        public IQueryable<Project> FindAll()
        {
            return db.Projects.AsNoTracking().AsQueryable();
        }
        /// <summary>
        /// Included Employees and users each of them
        /// </summary>
        /// <returns></returns>
        public IQueryable<Project> FindAllWithDetails()
        {
            return db.Projects.Include(p => p.Employees).ThenInclude(p => p.User).AsNoTracking().AsQueryable();
        }
        /// <summary>
        /// Finds project by project's property
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public IQueryable<Project> FindProjectByFilter(Func<Project, bool> predicate)
        {
            return db.Projects.Include(p => p.Employees).ThenInclude(p => p.User).AsNoTracking().Where(predicate).AsQueryable();
        }
        public async Task<Project> GetByIdAsync(int id)
        {
            return await db.Projects.AsNoTracking().FirstOrDefaultAsync(p => p.Id == id);
        }
        public void Update(Project entity)
        {
            db.Projects.Update(entity);
            db.SaveChanges();
        }
    }
}
