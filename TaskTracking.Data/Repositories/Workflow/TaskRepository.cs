﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using TaskTracking.Data.EF;
using TaskTracking.Data.Interfaces.Workflow;

namespace TaskTracking.Data.Repositories.Workflow
{
    public class TaskRepository : ITaskRepository
    {
        private readonly ApplicationDbContext db;
        public TaskRepository(ApplicationDbContext context)
        {
            db = context;
        }
        public async Task AddAsync(Entities.Workflow.Task entity)
        {
            await db.Tasks.AddAsync(entity);
            await db.SaveChangesAsync();
        }
        /// <summary>
        /// Task won't be deleted, but will be closed
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteByIdAsync(int id)
        {
            Entities.Workflow.Task task = await db.Tasks.FindAsync(id);
            if (task != null)
            {
                task.Status = Entities.Workflow.StatusType.Closed;
                db.Tasks.Update(task);
            }
            await db.SaveChangesAsync();
        }
        public IQueryable<Entities.Workflow.Task> FindAll()
        {
            return db.Tasks.AsNoTracking().AsQueryable();
        }
        /// <summary>
        /// Finds task by task's property
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public IQueryable<Entities.Workflow.Task> FindTaskByFilter(Func<Entities.Workflow.Task, bool> predicate)
        {
            return db.Tasks.AsNoTracking().Where(predicate).AsQueryable();
        }
        public async Task<Entities.Workflow.Task> GetByIdAsync(int id)
        {
            return await db.Tasks.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
        }

        public void Update(Entities.Workflow.Task entity)
        {
            db.Tasks.Update(entity);
            db.SaveChanges();
        }
    }
}
