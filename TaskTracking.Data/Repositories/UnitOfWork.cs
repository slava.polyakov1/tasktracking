﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Threading.Tasks;
using TaskTracking.Data.EF;
using TaskTracking.Data.Interfaces;
using TaskTracking.Data.Interfaces.Workflow;
using TaskTracking.Data.Repositories.Workflow;

namespace TaskTracking.Data.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext db;
        //public ApplicationSignInManager SignInManager { get; }
        //public ApplicationUserManager UserManager { get; }
        //public ApplicationRoleManager RoleManager { get; }
        private ClientRepository clientRepository;
        private EmployeeRepository employeeRepository;
        private ProjectRepository projectRepository;
        private TaskRepository taskRepository;
        private TaskHistoryRepository taskHistoryRepository;
        public IClientRepository ClientRepository => clientRepository ??= new ClientRepository(db);
        public IEmployeeRepository EmployeeRepository => employeeRepository ??= new EmployeeRepository(db);
        public IProjectRepository ProjectRepository => projectRepository ??= new ProjectRepository(db);
        public ITaskRepository TaskRepository => taskRepository ??= new TaskRepository(db);
        public ITaskHistoryRepository TaskHistoryRepository => taskHistoryRepository ??= new TaskHistoryRepository(db);
        public UnitOfWork(DbContextOptions<ApplicationDbContext> options)
        {
            db = new ApplicationDbContext(options);
        }

        public async Task<int> SaveAsync()
        {
            return await db.SaveChangesAsync();
        }
        public async Task<IDbContextTransaction> BeginTransactionAsync()
        {
            return await db.Database.BeginTransactionAsync();
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        private bool disposed = false;
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                    //UserManager.Dispose();
                    //RoleManager.Dispose();
                    //ClientManager.Dispose();
                }
                this.disposed = true;
            }
        }
    }
}
