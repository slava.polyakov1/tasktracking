﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using TaskTracking.Data.Entities.Identity;

namespace TaskTracking.Data.Configurations
{

    public class UsersWithRolesConfiguration : IEntityTypeConfiguration<ApplicationUserRole>
    {
        public static Guid RoleGuid = Guid.NewGuid();
        public static Guid AdminGuid = Guid.NewGuid();
        public void Configure(EntityTypeBuilder<ApplicationUserRole> builder)
        {
            ApplicationUserRole iur = new ApplicationUserRole
            {
                RoleId = RoleGuid,
                UserId = AdminGuid
            };

            builder.HasData(iur);
        }
    }
}
