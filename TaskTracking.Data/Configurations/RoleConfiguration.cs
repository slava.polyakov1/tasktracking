﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using TaskTracking.Data.Entities.Identity;

namespace TaskTracking.Data.Configurations
{
    public class RoleConfiguration : IEntityTypeConfiguration<ApplicationRole>
    {
        public void Configure(EntityTypeBuilder<ApplicationRole> builder)
        {
            builder.HasData(
                new ApplicationRole
                {
                    Id = UsersWithRolesConfiguration.RoleGuid,
                    Name = "Administrator",
                    NormalizedName = "ADMINISTRATOR"
                },
                new ApplicationRole
                {
                    Id = Guid.NewGuid(),
                    Name = "Manager",
                    NormalizedName = "MANAGER"
                },
                new ApplicationRole
                {
                    Id = Guid.NewGuid(),
                    Name = "Employee",
                    NormalizedName = "EMPLOYEE"
                },
                new ApplicationRole
                {
                    Id = Guid.NewGuid(),
                    Name = "Client",
                    NormalizedName = "CLIENT"
                },
                new ApplicationRole
                {
                    Id = Guid.NewGuid(),
                    Name = "User",
                    NormalizedName = "USER"
                }
            );
        }
    }
}
