﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using TaskTracking.Data.Configurations;
using TaskTracking.Data.Entities.Identity;
using TaskTracking.Data.Entities.Workflow;

namespace TaskTracking.Data.EF
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, Guid, IdentityUserClaim<Guid>, ApplicationUserRole, IdentityUserLogin<Guid>,
    IdentityRoleClaim<Guid>, IdentityUserToken<Guid>>
    {
        //public ApplicationDbContext() : base()
        //{

        //}
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<ApplicationUser>(b =>
            {
                b.HasMany(e => e.UserRoles)
                    .WithOne(e => e.User)
                    .HasForeignKey(ur => ur.UserId)
                    .IsRequired();
            });

            builder.Entity<ApplicationRole>(b =>
            {
                b.HasMany(e => e.UserRoles)
                    .WithOne(e => e.Role)
                    .HasForeignKey(ur => ur.RoleId)
                    .IsRequired();

            });
            builder.Entity<ApplicationUserRole>(userRole =>
            {
                //userRole.ToTable("AspNetUserRoles");
                userRole.HasKey(p => new { p.UserId, p.RoleId });

                userRole.HasOne(p => p.Role)
                       .WithMany(r => r.UserRoles)
                       .HasForeignKey(p => p.RoleId)
                       .IsRequired();

                userRole.HasOne(p => p.User)
                       .WithMany(r => r.UserRoles)
                       .HasForeignKey(p => p.UserId)
                       .IsRequired();
            });

            builder.Entity<Project>()
                .HasOne(b => b.Client)
                .WithOne(a => a.Project)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<TaskHistory>()
                .HasOne(b => b.Task)
                .WithMany(a => a.TaskHistories)
                .OnDelete(DeleteBehavior.Restrict);
            builder.ApplyConfiguration(new RoleConfiguration());
            builder.ApplyConfiguration(new AdminConfiguration());
            builder.ApplyConfiguration(new UsersWithRolesConfiguration());
        }
        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=TaskTrackingDb;Trusted_Connection=True;");
        //}
        public DbSet<Project> Projects { get; set; }
        public DbSet<TaskTracking.Data.Entities.Workflow.Task> Tasks { get; set; }
        public DbSet<TaskHistory> TaskHistories { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Employee> Employees { get; set; }
    }
}
