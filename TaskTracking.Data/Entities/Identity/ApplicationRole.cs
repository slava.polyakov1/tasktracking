﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace TaskTracking.Data.Entities.Identity
{
    public class ApplicationRole : IdentityRole<Guid>
    {
        public ICollection<ApplicationUserRole> UserRoles { get; set; }
    }
}
