﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using TaskTracking.Data.Entities.Workflow;

namespace TaskTracking.Data.Entities.Identity
{
    public class ApplicationUser : IdentityUser<Guid>
    {
        public virtual Client Client { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual ICollection<ApplicationUserRole> UserRoles { get; set; }
        //public virtual ICollection<IdentityUserClaim<Guid>> Claims { get; set; }
        //public virtual ICollection<IdentityUserLogin<Guid>> Logins { get; set; }
        //public virtual ICollection<IdentityUserToken<Guid>> Tokens { get; set; }
    }
}
