﻿using System;
using System.Collections.Generic;
using TaskTracking.Data.Entities.Identity;

namespace TaskTracking.Data.Entities.Workflow
{
    public class Employee : BaseEntity
    {
        public Guid UserId { get; set; }
        public ApplicationUser User { get; set; }
        public ICollection<Project> Projects { get; set; }
    }
}
