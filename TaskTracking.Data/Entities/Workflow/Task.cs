﻿using System;
using System.Collections.Generic;

namespace TaskTracking.Data.Entities.Workflow
{
    public enum StatusType
    {
        Opened = 0,
        InProgress,
        Closed
    }
    public class Task : BaseEntity
    {
        public string Title { get; set; }
        public int Progress { get; set; }
        public StatusType Status { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EstimatedDate { get; set; }
        public int ProjectId { get; set; }
        public int EmployeeId { get; set; }
        public Project Project { get; set; }
        public Employee Employee { get; set; }
        public ICollection<TaskHistory> TaskHistories { get; set; }
    }
}
