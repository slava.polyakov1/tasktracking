﻿using System;

namespace TaskTracking.Data.Entities.Workflow
{
    public class TaskHistory : BaseEntity
    {
        public DateTime StatusChangedDate { get; set; }
        public StatusType Status { get; set; }
        public int TaskId { get; set; }
        public int ProjectId { get; set; }
        public int EmployeeId { get; set; }
        public Project Project { get; set; }
        public Employee Employee { get; set; }
        public Task Task { get; set; }
    }
}
