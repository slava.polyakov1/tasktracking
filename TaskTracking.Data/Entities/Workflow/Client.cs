﻿using System;
using TaskTracking.Data.Entities.Identity;

namespace TaskTracking.Data.Entities.Workflow
{
    public class Client : BaseEntity
    {
        public string CompanyName { get; set; }
        public Guid UserId { get; set; }
        public ApplicationUser User { get; set; }
        public Project Project { get; set; }
    }
}
