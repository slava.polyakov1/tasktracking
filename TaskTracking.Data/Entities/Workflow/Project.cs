﻿using System.Collections.Generic;

namespace TaskTracking.Data.Entities.Workflow
{
    public class Project : BaseEntity
    {
        public string Title { get; set; }
        public ICollection<Employee> Employees { get; set; }
        public int ClientId { get; set; }
        public Client Client { get; set; }
    }
}
