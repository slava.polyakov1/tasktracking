﻿using System.ComponentModel.DataAnnotations;

namespace TaskTracking.Data.Entities
{
    public abstract class BaseEntity
    {
        [Key]
        public int Id { get; set; }
    }
}
