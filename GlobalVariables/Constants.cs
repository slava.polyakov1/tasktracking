﻿using System;

namespace TaskTracking.GlobalVariables
{
    public static class Constants
    {
        public const string SuccessfulRegister = "Для завершения регистрации проверьте электронную почту и перейдите по ссылке, указанной в письме";
        //public const string CONFIRM_EMAIL = $"Подтвердите регистрацию, перейдя по ссылке: <a href='{callbackUrl}'>link</a>";
        public const string AdminRole = "Administrator";
        public const string ManagerRole = "Manager";
        public const string EmployeeRole = "Employee";
        public const string ClientRole = "Client";
        public const string UserRole = "User";
        public const string AdminManager = AdminRole + "," + ManagerRole;
        public const string AdminManagerClient = AdminRole + "," + ManagerRole + "," + ClientRole;
        public const string AdminManagerEmployee = AdminRole + "," + ManagerRole + "," + EmployeeRole;
        public const string AdminClientUser = AdminRole + "," + ClientRole + "," + UserRole;
        public const string AdminEmployeeUser = AdminRole + "," + EmployeeRole + "," + UserRole;
        public const string AdminClient = AdminRole + "," + ClientRole;
        public const string AdminEmployee = AdminRole + "," + EmployeeRole;
    }
}
