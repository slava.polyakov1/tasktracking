﻿using AutoMapper;
using System.Linq;
using TaskTracking.Business.Models.Account;
using TaskTracking.Business.Models.Workflow;
using TaskTracking.Data.Entities.Identity;
using TaskTracking.Data.Entities.Workflow;

namespace TaskTracking.Business.Mapper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Project, ProjectDTO>()
                .ForMember(c => c.Employees, c => c.MapFrom(emp => emp.Employees.Select(e => e.Id).ToList()))
                .ReverseMap().MaxDepth(1);
            CreateMap<Employee, EmployeeDTO>()
                .ForMember(c => c.Projects, c => c.MapFrom(emp => emp.Projects.Select(p => p.Id).ToList()))
                .ReverseMap().MaxDepth(1);
            CreateMap<Client, ClientDTO>()
                //.ForMember(c => c.ProjectId, c => c.MapFrom(c => c.Project.Id))
                .ReverseMap().MaxDepth(1);
            CreateMap<Task, TaskDTO>()
                .ForMember(c => c.TaskHistoriesIds, c => c.MapFrom(t => t.TaskHistories))
                .ReverseMap().MaxDepth(1);
            CreateMap<TaskHistory, TaskHistoryDTO>()
                .ForMember(c => c.TaskId, c => c.MapFrom(t => t.Task.Id))
                .ReverseMap().MaxDepth(1);
            CreateMap<ApplicationUser, UserDTO>()
                .ForMember(r => r.Password, r => r.MapFrom(u => u.PasswordHash))
                .ReverseMap();
            CreateMap<ApplicationUser, RegisterDTO>()
                .ForMember(r => r.Password, r => r.MapFrom(u => u.PasswordHash))
                .ReverseMap();
        }
    }
}
