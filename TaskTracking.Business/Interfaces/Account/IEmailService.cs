﻿using System.Threading.Tasks;

namespace TaskTracking.Business.Interfaces.Account
{
    public interface IEmailService
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
