﻿using System.Threading.Tasks;
using TaskTracking.Business.Infrastructure;
using TaskTracking.Business.Models.Account;
using TaskTracking.Data.Entities.Identity;

namespace TaskTracking.Business.Interfaces.Account
{
    public interface IAuthService
    {
        Task<OperationDetails> CreateUser(RegisterDTO userDto);
        Task<string> GenerateJwt(ApplicationUser user);
    }
}
