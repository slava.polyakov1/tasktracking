﻿using System;
using System.Collections.Generic;
using TaskTracking.Business.Models.Workflow;

namespace TaskTracking.Business.Interfaces.Workflow
{
    public interface IProjectService : ICrud<ProjectDTO>
    {
        IEnumerable<ProjectDTO> FindProjectByFilter(Func<ProjectDTO, bool> predicate);
        IEnumerable<ProjectDTO> GetAllWithDetails();
    }
}
