﻿using System;
using TaskTracking.Business.Models.Workflow;

namespace TaskTracking.Business.Interfaces.Workflow
{
    public interface IClientService : ICrud<ClientDTO>
    {
        System.Threading.Tasks.Task<ClientDTO> GetByUserIdAsync(Guid userId);
        System.Threading.Tasks.Task UpdateCompanyNameAsync(string companyName, int id);
    }
}
