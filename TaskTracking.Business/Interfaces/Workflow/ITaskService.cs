﻿using System.Collections.Generic;
using TaskTracking.Business.Models.Workflow;

namespace TaskTracking.Business.Interfaces.Workflow
{
    public interface ITaskService : ICrud<TaskDTO>
    {
        IEnumerable<TaskDTO> GetTasksByProjectId(int projectId);
        System.Threading.Tasks.Task UpdateProgressAsync(int progress, int taskId);
    }
}
