﻿using TaskTracking.Business.Models.Workflow;

namespace TaskTracking.Business.Interfaces.Workflow
{
    public interface ITaskHistoryService : ICrud<TaskHistoryDTO>
    {

    }
}
