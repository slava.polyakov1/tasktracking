﻿using System;
using TaskTracking.Business.Models.Workflow;

namespace TaskTracking.Business.Interfaces.Workflow
{
    public interface IEmployeeService : ICrud<EmployeeDTO>
    {
        System.Threading.Tasks.Task<EmployeeDTO> GetByUserIdAsync(Guid userId);
    }
}
