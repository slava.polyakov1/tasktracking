﻿using System.Collections.Generic;

namespace TaskTracking.Business.Infrastructure
{
    public class OperationDetails
    {
        public OperationDetails(bool succeeded, IEnumerable<string> message = null)
        {
            Succeeded = succeeded;
            Message = message;
        }
        public bool Succeeded { get; private set; }
        public IEnumerable<string> Message { get; private set; }
    }
}
