﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using TaskTracking.Business.Infrastructure;
using TaskTracking.Business.Interfaces.Account;
using TaskTracking.Business.Models.Account;
using TaskTracking.Business.Settings;
using TaskTracking.Business.Validation;
using TaskTracking.Data.Entities.Identity;
using TaskTracking.Data.Interfaces;

namespace TaskTracking.Business.Services.Account
{
    public class AuthService : IAuthService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly JwtSettings _jwtSettings;
        private IUnitOfWork _unitOfWork { get; set; }
        private readonly IMapper _mapper;
        public AuthService(UserManager<ApplicationUser> userManager,
            IOptionsSnapshot<JwtSettings> jwtSettings,
            IUnitOfWork unitOfWork,
            IMapper mapper)
        {
            _userManager = userManager;
            _jwtSettings = jwtSettings.Value;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        /// <summary>
        /// Registed user with role "User"
        /// </summary>
        /// <param name="userDto"></param>
        /// <returns></returns>
        public async Task<OperationDetails> CreateUser(RegisterDTO userDto)
        {
            var userMapped = _mapper.Map<RegisterDTO, ApplicationUser>(userDto);
            ApplicationUser user = await _userManager.FindByEmailAsync(userMapped.Email);
            if (user == null)
            {
                using (var transaction = await _unitOfWork.BeginTransactionAsync())
                {
                    try
                    {
                        var result = await _userManager.CreateAsync(userMapped, userDto.Password);
                        if (!result.Succeeded)
                            throw new TrackValidationException(result.Errors.Select(x => x.Description).ToString());
                        //return new OperationDetails(false, result.Errors.Select(x => x.Description));

                        await _userManager.AddToRoleAsync(userMapped, "User");
                        await transaction.CommitAsync();
                        return new OperationDetails(true);
                    }
                    catch (Exception)
                    {
                        await transaction.RollbackAsync();
                        throw new TrackValidationException();
                    }
                }
            }
            return new OperationDetails(false, new[] { "Пользователь с таким логином уже существует" });
        }
        /// <summary>
        /// Generating token for user. Includes UserId, email, UserName and roles
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<string> GenerateJwt(ApplicationUser user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.Sub, user.Email),
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var roles = await _userManager.GetRolesAsync(user);

            claims.AddRange(roles.Select(role => new Claim(ClaimsIdentity.DefaultRoleClaimType, role)));

            var key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_jwtSettings.Secret));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddMinutes(Convert.ToDouble(_jwtSettings.LifeTime));

            var token = new JwtSecurityToken(
                _jwtSettings.Issuer,
                _jwtSettings.Issuer,
                claims,
                expires: expires,
                signingCredentials: creds
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
