﻿using MailKit.Net.Smtp;
using MimeKit;
using System.Threading.Tasks;
using TaskTracking.Business.Interfaces.Account;

namespace TaskTracking.Business.Services.Account
{
    public class EmailService : IEmailService
    {
        /// <summary>
        /// Send mail. For mail confirm or notifications
        /// </summary>
        /// <param name="email">Mail to(Destination)</param>
        /// <param name="subject"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public async Task SendEmailAsync(string email, string subject, string message)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("Администрация сайта", "slava.polyakov.1997@gmail.com"));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = message
            };

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync("smtp.gmail.com", 587, MailKit.Security.SecureSocketOptions.StartTlsWhenAvailable);
                await client.AuthenticateAsync("slava.polyakov.1997@gmail.com", "slava1997");
                await client.SendAsync(emailMessage);

                await client.DisconnectAsync(true);
            }
        }
    }
}
