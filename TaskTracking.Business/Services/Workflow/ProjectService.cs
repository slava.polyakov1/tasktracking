﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TaskTracking.Business.Interfaces.Workflow;
using TaskTracking.Business.Models.Workflow;
using TaskTracking.Data.Entities.Workflow;
using TaskTracking.Data.Interfaces;

namespace TaskTracking.Business.Services.Workflow
{
    public class ProjectService : IProjectService
    {
        private IUnitOfWork _unitOfWork { get; set; }
        private readonly IMapper _mapper;
        public ProjectService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async System.Threading.Tasks.Task AddAsync(ProjectDTO model)
        {
            var project = _mapper.Map<Project>(model);

            await _unitOfWork.ProjectRepository.AddAsync(project);
            await _unitOfWork.SaveAsync();
        }
        public async System.Threading.Tasks.Task DeleteByIdAsync(int modelId)
        {
            await _unitOfWork.ProjectRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }
        public IEnumerable<ProjectDTO> GetAll()
        {
            return _mapper.Map<IEnumerable<ProjectDTO>>(_unitOfWork.ProjectRepository.FindAll());
        }
        /// <summary>
        /// Get all projects with included employees and users
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ProjectDTO> GetAllWithDetails()
        {
            return _mapper.Map<IEnumerable<ProjectDTO>>(_unitOfWork.ProjectRepository.FindAllWithDetails());
        }
        public async Task<ProjectDTO> GetByIdAsync(int id)
        {
            var project = await _unitOfWork.ProjectRepository.GetByIdAsync(id);
            return _mapper.Map<ProjectDTO>(project);
        }
        /// <summary>
        /// Find's project by project's property
        /// </summary>
        /// <param name="predicateDto"></param>
        /// <returns></returns>
        public IEnumerable<ProjectDTO> FindProjectByFilter(Func<ProjectDTO, bool> predicateDto)
        {
            var predicate = _mapper.Map<Func<Project, bool>>(predicateDto);
            return _mapper.Map<IEnumerable<ProjectDTO>>(_unitOfWork.ProjectRepository.FindProjectByFilter(predicate));
        }
        public async System.Threading.Tasks.Task UpdateAsync(ProjectDTO model)
        {
            var project = _mapper.Map<Project>(model);
            _unitOfWork.ProjectRepository.Update(project);
            await _unitOfWork.SaveAsync();
        }
    }
}
