﻿using AutoMapper;
using System.Collections.Generic;
using TaskTracking.Business.Interfaces.Workflow;
using TaskTracking.Business.Models.Workflow;
using TaskTracking.Data.Entities.Workflow;
using TaskTracking.Data.Interfaces;

namespace TaskTracking.Business.Services.Workflow
{
    public class TaskHistoryService : ITaskHistoryService
    {
        private IUnitOfWork _unitOfWork { get; set; }
        private readonly IMapper _mapper;
        public TaskHistoryService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async System.Threading.Tasks.Task AddAsync(TaskHistoryDTO model)
        {
            var taskHistory = _mapper.Map<TaskHistory>(model);

            await _unitOfWork.TaskHistoryRepository.AddAsync(taskHistory);
            await _unitOfWork.SaveAsync();
        }

        public async System.Threading.Tasks.Task DeleteByIdAsync(int modelId)
        {
            await _unitOfWork.TaskHistoryRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }

        public IEnumerable<TaskHistoryDTO> GetAll()
        {
            return _mapper.Map<IEnumerable<TaskHistoryDTO>>(_unitOfWork.TaskHistoryRepository.FindAll());
        }

        public async System.Threading.Tasks.Task<TaskHistoryDTO> GetByIdAsync(int id)
        {
            var taskHistory = await _unitOfWork.TaskHistoryRepository.GetByIdAsync(id);
            return _mapper.Map<TaskHistoryDTO>(taskHistory);
        }

        public async System.Threading.Tasks.Task UpdateAsync(TaskHistoryDTO model)
        {
            var taskHistory = _mapper.Map<TaskHistory>(model);
            _unitOfWork.TaskHistoryRepository.Update(taskHistory);
            await _unitOfWork.SaveAsync();
        }
    }
}
