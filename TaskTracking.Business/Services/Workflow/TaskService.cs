﻿using AutoMapper;
using System;
using System.Collections.Generic;
using TaskTracking.Business.Interfaces.Workflow;
using TaskTracking.Business.Models.Workflow;
using TaskTracking.Business.Validation;
using TaskTracking.Data.Entities.Workflow;
using TaskTracking.Data.Interfaces;

namespace TaskTracking.Business.Services.Workflow
{
    public class TaskService : ITaskService
    {
        private IUnitOfWork _unitOfWork { get; set; }
        private readonly IMapper _mapper;
        public TaskService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async System.Threading.Tasks.Task AddAsync(TaskDTO model)
        {
            var task = _mapper.Map<Task>(model);
            if (task.StartDate == DateTime.MinValue)
                task.StartDate = DateTime.Now;

            await _unitOfWork.TaskRepository.AddAsync(task);
            await _unitOfWork.SaveAsync();
        }
        /// <summary>
        /// Task won't be deleted, but will be closed
        /// </summary>
        /// <param name="modelId"></param>
        /// <returns></returns>
        public async System.Threading.Tasks.Task DeleteByIdAsync(int modelId)
        {
            await _unitOfWork.TaskRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }
        public IEnumerable<TaskDTO> GetAll()
        {
            return _mapper.Map<IEnumerable<TaskDTO>>(_unitOfWork.TaskRepository.FindAll());
        }
        public IEnumerable<TaskDTO> GetTasksByProjectId(int projectId)
        {
            return _mapper.Map<IEnumerable<TaskDTO>>(_unitOfWork.TaskRepository.FindTaskByFilter(x => x.ProjectId == projectId));
        }
        public async System.Threading.Tasks.Task<TaskDTO> GetByIdAsync(int id)
        {
            var task = await _unitOfWork.TaskRepository.GetByIdAsync(id);
            return _mapper.Map<TaskDTO>(task);
        }
        public async System.Threading.Tasks.Task UpdateAsync(TaskDTO model)
        {
            var task = _mapper.Map<Task>(model);
            _unitOfWork.TaskRepository.Update(task);
            await _unitOfWork.SaveAsync();
        }
        /// <summary>
        /// Update task progress
        /// </summary>
        /// <param name="progress">Progress should be more than 0 and less or equal than 100</param>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public async System.Threading.Tasks.Task UpdateProgressAsync(int progress, int taskId)
        {
            var task = await _unitOfWork.TaskRepository.GetByIdAsync(taskId);
            if (task == null)
                throw new TrackValidationException("Not found task by this id");

            if (progress <= 0 || progress > 100)
                throw new TrackValidationException("Progress should be more than 0 and less or equal than 100");

            task.Progress = progress;
            _unitOfWork.TaskRepository.Update(task);
        }
    }
}
