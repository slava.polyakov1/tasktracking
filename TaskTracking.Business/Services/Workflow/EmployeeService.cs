﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskTracking.Business.Interfaces.Workflow;
using TaskTracking.Business.Models.Workflow;
using TaskTracking.Business.Validation;
using TaskTracking.Data.Entities.Identity;
using TaskTracking.Data.Entities.Workflow;
using TaskTracking.Data.Interfaces;

namespace TaskTracking.Business.Services.Workflow
{
    public class EmployeeService : IEmployeeService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private IUnitOfWork _unitOfWork { get; set; }
        private readonly IMapper _mapper;
        public EmployeeService(UserManager<ApplicationUser> userManager,
            IUnitOfWork unitOfWork, IMapper mapper)
        {
            _userManager = userManager;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async System.Threading.Tasks.Task AddAsync(EmployeeDTO model)
        {
            var employee = _mapper.Map<Employee>(model);
            Employee employeeExists = await _unitOfWork.EmployeeRepository.GetByUserIdAsync(employee.UserId);
            if (employeeExists != null)
                throw new TrackValidationException("Employee is already exists");

            await _unitOfWork.EmployeeRepository.AddAsync(employee);
            await _unitOfWork.SaveAsync();
        }
        public async System.Threading.Tasks.Task DeleteByIdAsync(int modelId)
        {
            await _unitOfWork.EmployeeRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }
        public IEnumerable<EmployeeDTO> GetAll()
        {
            return _mapper.Map<IEnumerable<EmployeeDTO>>(_unitOfWork.EmployeeRepository.FindAll());
        }
        public async Task<EmployeeDTO> GetByUserIdAsync(Guid userId)
        {
            var employee = await _unitOfWork.EmployeeRepository.GetByUserIdAsync(userId);
            return _mapper.Map<EmployeeDTO>(employee);
        }
        public async Task<EmployeeDTO> GetByIdAsync(int id)
        {
            var employee = await _unitOfWork.EmployeeRepository.GetByIdAsync(id);
            return _mapper.Map<EmployeeDTO>(employee);
        }
        /// <summary>
        /// Update employee and his projects
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async System.Threading.Tasks.Task UpdateAsync(EmployeeDTO model)
        {
            var employee = _mapper.Map<Employee>(model);
            Employee employeeExists = await _unitOfWork.EmployeeRepository.GetByUserIdAsync(employee.UserId);
            if (employeeExists == null)
                throw new TrackValidationException("Employee not exist");
            var projects = _unitOfWork.ProjectRepository.FindAll().Where(p => model.Projects.Contains(p.Id));
            employee.Projects = projects.ToList();
            _unitOfWork.EmployeeRepository.Update(employee);
            await _unitOfWork.SaveAsync();
        }
    }
}
