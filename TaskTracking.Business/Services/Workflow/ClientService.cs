﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TaskTracking.Business.Interfaces.Workflow;
using TaskTracking.Business.Models.Workflow;
using TaskTracking.Business.Validation;
using TaskTracking.Data.Entities.Identity;
using TaskTracking.Data.Entities.Workflow;
using TaskTracking.Data.Interfaces;
using TaskTracking.GlobalVariables;

namespace TaskTracking.Business.Services.Workflow
{
    public class ClientService : IClientService
    {
        private IUnitOfWork _unitOfWork { get; set; }
        private readonly IMapper _mapper;
        private readonly UserManager<ApplicationUser> _userManager;
        public ClientService(IUnitOfWork unitOfWork, IMapper mapper, UserManager<ApplicationUser> userManager)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _userManager = userManager;
        }
        public async System.Threading.Tasks.Task AddAsync(ClientDTO model)
        {
            var client = _mapper.Map<Client>(model);
            Client clientExists = await _unitOfWork.ClientRepository.GetByUserIdAsync(client.UserId);
            if (clientExists != null)
                throw new TrackValidationException("Client is already exists");

            await _unitOfWork.ClientRepository.AddAsync(client);
            await _unitOfWork.SaveAsync();
        }
        public IEnumerable<ClientDTO> GetAll()
        {
            return _mapper.Map<IEnumerable<ClientDTO>>(_unitOfWork.ClientRepository.FindAll());
        }
        public async Task<ClientDTO> GetByUserIdAsync(Guid userId)
        {
            return _mapper.Map<ClientDTO>(await _unitOfWork.ClientRepository.GetByUserIdAsync(userId));
        }
        public async Task<ClientDTO> GetByIdAsync(int id)
        {
            var client = await _unitOfWork.ClientRepository.GetByIdAsync(id);
            return _mapper.Map<ClientDTO>(client);
        }
        public async System.Threading.Tasks.Task UpdateCompanyNameAsync(string companyName, int id)
        {
            var client = await _unitOfWork.ClientRepository.GetByIdAsync(id);
            if (client == null)
                throw new TrackValidationException("Client is not exist");

            client.CompanyName = companyName;
            _unitOfWork.ClientRepository.Update(client);
            await _unitOfWork.SaveAsync();
        }
        public async System.Threading.Tasks.Task UpdateAsync(ClientDTO model)
        {
            var client = _mapper.Map<Client>(model);
            if (client == null)
                throw new TrackValidationException("Client is not exist");

            _unitOfWork.ClientRepository.Update(client);
            await _unitOfWork.SaveAsync();
        }
        public async System.Threading.Tasks.Task DeleteByIdAsync(int modelId)
        {
            var client = await _unitOfWork.ClientRepository.GetByIdAsync(modelId);
            var user = await _userManager.FindByIdAsync(client.UserId.ToString());

            await _unitOfWork.ClientRepository.DeleteByIdAsync(modelId);
            await _userManager.RemoveFromRoleAsync(user, Constants.ClientRole);
            await _unitOfWork.SaveAsync();
        }
    }
}
