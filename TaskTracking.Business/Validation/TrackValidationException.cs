﻿using System;
using System.Runtime.Serialization;

namespace TaskTracking.Business.Validation
{
    [Serializable]
    public class TrackValidationException : Exception
    {
        public TrackValidationException() : base() { }
        public TrackValidationException(string message) : base(message) { }
        public TrackValidationException(string message, Exception inner) : base(message, inner) { }
        public TrackValidationException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
