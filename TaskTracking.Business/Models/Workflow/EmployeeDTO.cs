﻿using System;
using System.Collections.Generic;

namespace TaskTracking.Business.Models.Workflow
{
    public class EmployeeDTO
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }
        public ICollection<int> Projects { get; set; }
    }
}
