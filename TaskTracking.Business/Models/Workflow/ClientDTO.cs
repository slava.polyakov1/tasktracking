﻿using System;

namespace TaskTracking.Business.Models.Workflow
{
    public class ClientDTO
    {
        public int Id { get; set; }
        public string CompanyName { get; set; }
        public Guid UserId { get; set; }
        public int ProjectId { get; set; }
    }
}
