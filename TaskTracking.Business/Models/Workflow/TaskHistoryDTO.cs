﻿using System;
using TaskTracking.Data.Entities.Workflow;

namespace TaskTracking.Business.Models.Workflow
{
    public class TaskHistoryDTO
    {
        public int Id { get; set; }
        public DateTime StatusChangedDate { get; set; }
        public StatusType Status { get; set; }
        public int ProjectId { get; set; }
        public int EmployeeId { get; set; }
        public int TaskId { get; set; }
    }
}
