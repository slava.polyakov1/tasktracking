﻿using System.Collections.Generic;

namespace TaskTracking.Business.Models.Workflow
{
    public class ProjectDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int ClientId { get; set; }
        public ICollection<int> Employees { get; set; }
    }
}
