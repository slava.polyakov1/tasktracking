﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TaskTracking.Data.Entities.Workflow;

namespace TaskTracking.Business.Models.Workflow
{
    public class TaskDTO
    {
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        public int Progress { get; set; }
        public StatusType Status { get; set; }
        public DateTime StartDate { get; set; }
        [Required]
        public DateTime EstimatedDate { get; set; }
        [Required]
        public int ProjectId { get; set; }
        [Required]
        public int EmployeeId { get; set; }
        public ICollection<int> TaskHistoriesIds { get; set; }
    }
}
