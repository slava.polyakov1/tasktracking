﻿using System.ComponentModel.DataAnnotations;

namespace TaskTracking.Business.Models.Account
{
    public class RegisterDTO
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        //[Required]
        //[Compare("Password", ErrorMessage = "Пароли не совпадают")]
        //[DataType(DataType.Password)]
        //public string PasswordConfirm { get; set; }
    }
}
