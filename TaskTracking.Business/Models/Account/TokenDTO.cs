﻿using System.Text.Json.Serialization;

namespace TaskTracking.Business.Models.Account
{
    public class TokenDTO
    {
        [JsonPropertyName("token_type")]
        public string TokenType { get; set; }
        [JsonPropertyName("token")]
        public string Token { get; set; }
    }
}
