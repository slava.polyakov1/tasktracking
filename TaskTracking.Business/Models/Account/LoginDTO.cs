﻿using System.ComponentModel.DataAnnotations;

namespace TaskTracking.Business.Models.Account
{
    public class LoginDTO
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
