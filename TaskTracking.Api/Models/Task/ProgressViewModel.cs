﻿namespace TaskTracking.Api.Models.Task
{
    public class ProgressViewModel
    {
        public int Progress { get; set; }
    }
}
