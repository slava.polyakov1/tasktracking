﻿using System.ComponentModel.DataAnnotations;

namespace TaskTracking.Api.Models.Project
{
    public class ProjectCreateViewModel
    {
        [Required]
        public string UserId { get; set; }
        [Required]
        public string Title { get; set; }
    }
}
