﻿using System.ComponentModel.DataAnnotations;

namespace TaskTracking.Api.Models.Project
{
    public class ProjectFilterViewModel
    {
        public string Title { get; set; }
        [Required]
        public int ClientId { get; set; }
    }
}
