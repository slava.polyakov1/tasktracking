﻿namespace TaskTracking.Api.Models.Client
{
    public class ClientCompanyNameViewModel
    {
        public string CompanyName { get; set; }
    }
}
