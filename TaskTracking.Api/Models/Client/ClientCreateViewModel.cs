﻿using System.ComponentModel.DataAnnotations;

namespace TaskTracking.Api.Models.CLient
{
    public class ClientCreateViewModel
    {
        [Required]
        public string UserId { get; set; }
        [Required]
        public string CompanyName { get; set; }
    }
}
