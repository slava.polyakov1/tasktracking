﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TaskTracking.Api.Models.User
{
    public class UserRoleViewModel
    {
        [Required]
        public string UserId { get; set; }
        [Required]
        public List<string> Roles { get; set; }
    }
}
