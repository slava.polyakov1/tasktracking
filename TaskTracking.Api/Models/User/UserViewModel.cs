﻿using System.ComponentModel.DataAnnotations;

namespace TaskTracking.Api.Models
{
    public class UserViewModel
    {
        [Required]
        public string UserId { get; set; }
    }
}
