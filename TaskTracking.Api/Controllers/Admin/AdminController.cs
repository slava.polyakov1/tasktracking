﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskTracking.Api.Models.User;
using TaskTracking.Data.Entities.Identity;
using TaskTracking.GlobalVariables;

namespace TaskTracking.Api.Controllers.Admin
{
    [Authorize(Roles = Constants.AdminRole)]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        public AdminController(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }
        [HttpGet("users/roles")]
        public IActionResult Get()
        {
            List<object> resultList = new List<object>();
            var users = _userManager.Users.Include(x => x.UserRoles).ThenInclude(x => x.Role).ToList();
            foreach (var user in users)
            {
                List<object> roleIdAndNameList = new List<object>();
                user.UserRoles.ToList().ForEach(r => roleIdAndNameList.Add(new
                {
                    r.Role.Name,
                    r.RoleId
                }));
                var userWithRole = new
                {
                    userName = user.UserName,
                    userId = user.Id,
                    role = roleIdAndNameList
                };
                resultList.Add(userWithRole);
            }
            return Ok(new { data = resultList.ToList() });
        }
        [HttpPost("role/assign")]
        public async Task<IActionResult> AssignRole([FromBody] UserRoleViewModel userRole)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var user = await _userManager.FindByIdAsync(userRole.UserId);
                    if (user == null)
                        return BadRequest($"User: {user} was not found");

                    if (userRole.Roles.Count > 0)
                    {
                        var userRoles = await _userManager.GetRolesAsync(user);
                        var addedRoles = userRole.Roles.Except(userRoles);
                        var removedRoles = userRoles.Except(userRole.Roles);

                        await _userManager.AddToRolesAsync(user, addedRoles);
                        await _userManager.RemoveFromRolesAsync(user, removedRoles);
                        return Ok();
                    }
                    return BadRequest($"Role's field is empty");
                }
                catch (Exception)
                {
                    return Problem(statusCode: 500);
                }
            }
            var problemDetails = new ValidationProblemDetails(ModelState)
            {
                Status = StatusCodes.Status400BadRequest,
            };
            return ValidationProblem(problemDetails);
        }
    }
}
