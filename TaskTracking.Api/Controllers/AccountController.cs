﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using TaskTracking.Business.Interfaces.Account;
using TaskTracking.Business.Models.Account;
using TaskTracking.Business.Validation;
using TaskTracking.Data.Entities.Identity;
using TaskTracking.GlobalVariables;

namespace TaskTracking.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailService _emailService;
        private readonly IAuthService _authService;
        public AccountController(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IEmailService emailService,
            IAuthService authService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailService = emailService;
            _authService = authService;
        }
        [HttpPost("login")]
        public async Task<IActionResult> Login(LoginDTO model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = await _signInManager.PasswordSignInAsync(model.UserName, model.Password, true, false);
                    if (result.Succeeded)
                    {
                        var user = await _userManager.FindByNameAsync(model.UserName);
                        var token = await _authService.GenerateJwt(user);
                        return Ok(new { access_token = token });
                    }
                    else
                    {
                        return Forbid();
                    }
                }
                var problemDetails = new ValidationProblemDetails(ModelState)
                {
                    Status = StatusCodes.Status400BadRequest,
                };
                return ValidationProblem(problemDetails);
            }
            catch (Exception)
            {
                return Problem(statusCode: 500);
            }
        }
        [HttpPost("registration")]
        public async Task<IActionResult> Register(RegisterDTO model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = await _authService.CreateUser(model);
                    if (result.Succeeded)
                    {
                        var user = await _userManager.FindByEmailAsync(model.Email);
                        var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                        var callbackUrl = Url.Action(
                            "ConfirmEmail",
                            "Account",
                            new { userId = user.Id, code },
                            protocol: HttpContext.Request.Scheme);

                        await _emailService.SendEmailAsync(model.Email, "Confirm your account",
                            $"Подтвердите регистрацию, перейдя по ссылке: <a href='{callbackUrl}'>link</a>");

                        return StatusCode(StatusCodes.Status201Created, Constants.SuccessfulRegister);
                    }
                    else
                    {
                        foreach (var error in result.Message)
                        {
                            ModelState.AddModelError(string.Empty, error);
                        }
                    }
                }
                var problemDetails = new ValidationProblemDetails(ModelState)
                {
                    Status = StatusCodes.Status400BadRequest,
                };
                return ValidationProblem(problemDetails);
            }
            catch (TrackValidationException ex)
            {
                return Problem(statusCode: 400, detail: ex.Message);
            }
            catch (Exception)
            {
                return Problem(statusCode: 500);
            }
        }
        [HttpGet("confirm-email")]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            try
            {
                if (userId == null || code == null)
                    return BadRequest();

                var user = await _userManager.FindByIdAsync(userId);
                if (user == null)
                    return BadRequest();

                var result = await _userManager.ConfirmEmailAsync(user, code);
                if (result.Succeeded)
                    return Ok();
                else
                    return BadRequest();
            }
            catch (Exception)
            {
                return Problem(statusCode: 500);
            }
        }
    }
}
