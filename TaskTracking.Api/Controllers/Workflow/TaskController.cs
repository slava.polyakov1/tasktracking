﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using TaskTracking.Api.Models.Task;
using TaskTracking.Business.Interfaces.Account;
using TaskTracking.Business.Interfaces.Workflow;
using TaskTracking.Business.Models.Workflow;
using TaskTracking.Business.Validation;
using TaskTracking.Data.Entities.Identity;
using TaskTracking.GlobalVariables;

namespace TaskTracking.Api.Controllers.Workflow
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IEmailService _emailService;
        private readonly ITaskService _taskService;
        private readonly IClientService _clientService;
        private readonly IProjectService _projectService;
        private readonly IEmployeeService _employeeService;
        public TaskController(UserManager<ApplicationUser> userManager,
            IEmailService emailService, ITaskService taskService,
            IClientService clientService, IProjectService projectService,
            IEmployeeService employeeService)
        {
            _userManager = userManager;
            _emailService = emailService;
            _taskService = taskService;
            _clientService = clientService;
            _projectService = projectService;
            _employeeService = employeeService;
        }
        [Authorize(Roles = Constants.AdminManager)]
        [HttpGet("all")]
        public IActionResult GetTasks()
        {
            try
            {
                var tasks = _taskService.GetAll();
                return Ok(new { data = tasks });
            }
            catch (Exception)
            {
                return Problem(statusCode: 500);
            }
        }
        [Authorize(Roles = Constants.AdminManager)]
        [HttpGet("project/{id}")]
        public IActionResult GetTasksByProjectId(int id)
        {
            try
            {
                var tasks = _taskService.GetTasksByProjectId(id);
                return Ok(tasks);
            }
            catch (Exception)
            {
                return Problem(statusCode: 500);
            }
        }
        [Authorize(Roles = Constants.AdminManager)]
        [HttpPost("")]
        public async Task<IActionResult> Create([FromBody] TaskDTO model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _taskService.AddAsync(model);
                    var project = await _projectService.GetByIdAsync(model.ProjectId);
                    var client = await _clientService.GetByIdAsync(project.ClientId);
                    var userClient = await _userManager.FindByIdAsync(client.UserId.ToString());
                    var employee = await _employeeService.GetByIdAsync(model.EmployeeId);
                    var userEmployee = await _userManager.FindByIdAsync(employee.UserId.ToString());
                    await _emailService.SendEmailAsync(userClient.Email, "Task was assigned to user " + userEmployee.UserName, "");
                    return StatusCode(201);
                }
                catch (Exception)
                {
                    return Problem(statusCode: 500);
                }
            }
            var problemDetails = new ValidationProblemDetails(ModelState)
            {
                Status = StatusCodes.Status400BadRequest,
            };
            return ValidationProblem(problemDetails);
        }
        [Authorize(Roles = Constants.AdminManagerEmployee)]
        [HttpPut("progress/{id}")]
        public async Task<IActionResult> UpdateProgress(ProgressViewModel model, int id)
        {
            try
            {
                await _taskService.UpdateProgressAsync(model.Progress, id);
                return Ok();
            }
            catch (TrackValidationException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception)
            {
                return Problem(statusCode: 500);
            }
        }
        [Authorize(Roles = Constants.AdminManager)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _taskService.DeleteByIdAsync(id);
                return Ok();
            }
            catch (Exception)
            {
                return Problem(statusCode: 500);
            }
        }
    }
}
