﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using TaskTracking.Business.Interfaces.Workflow;
using TaskTracking.Business.Models.Workflow;
using TaskTracking.GlobalVariables;

namespace TaskTracking.Api.Controllers.Workflow
{
    [Authorize(Roles = Constants.AdminManager)]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class TaskHistoryController : ControllerBase
    {
        private readonly ITaskHistoryService _taskHistoryService;
        public TaskHistoryController(ITaskHistoryService taskHistoryService)
        {
            _taskHistoryService = taskHistoryService;
        }
        [HttpGet("all")]
        public IActionResult Get()
        {
            try
            {
                var history = _taskHistoryService.GetAll();
                return Ok();
            }
            catch (Exception)
            {
                return Problem(statusCode: 500);
            }
        }
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] TaskHistoryDTO taskHistoryDTO)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _taskHistoryService.AddAsync(taskHistoryDTO);
                    return StatusCode(201);
                }
                catch (Exception)
                {
                    return Problem(statusCode: 500);
                }
            }
            var problemDetails = new ValidationProblemDetails(ModelState)
            {
                Status = StatusCodes.Status400BadRequest,
            };
            return ValidationProblem(problemDetails);
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _taskHistoryService.DeleteByIdAsync(id);
                return Ok();
            }
            catch (Exception)
            {
                return Problem(statusCode: 500);
            }
        }
    }
}
