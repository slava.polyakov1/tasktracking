﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using TaskTracking.Api.Models.Project;
using TaskTracking.Business.Interfaces.Workflow;
using TaskTracking.Business.Models.Workflow;
using TaskTracking.Business.Validation;
using TaskTracking.Data.Entities.Identity;
using TaskTracking.GlobalVariables;

namespace TaskTracking.Api.Controllers.Workflow
{
    [Authorize(Roles = Constants.AdminManager)]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IProjectService _projectService;
        private readonly IClientService _clientService;
        public ProjectController(UserManager<ApplicationUser> userManager,
            IProjectService projectService, IClientService clientService)
        {
            _userManager = userManager;
            _projectService = projectService;
            _clientService = clientService;
        }
        [HttpGet("all/details")]
        public IActionResult GetAllWithDetails()
        {
            try
            {
                var projects = _projectService.GetAllWithDetails().ToList();
                return Ok(new { data = projects });
            }
            catch (TrackValidationException ex)
            {
                return Problem(statusCode: 500, detail: ex.Message);
            }
            catch (Exception)
            {
                return Problem(statusCode: 500);
            }
        }
        [HttpGet("all")]
        public IActionResult GetAll()
        {
            try
            {
                var projects = _projectService.GetAll().ToList();
                return Ok(new { data = projects });
            }
            catch (TrackValidationException ex)
            {
                return Problem(statusCode: 500, detail: ex.Message);
            }
            catch (Exception)
            {
                return Problem(statusCode: 500);
            }
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            try
            {
                var project = await _projectService.GetByIdAsync(id);
                return Ok(new { data = project });
            }
            catch (TrackValidationException ex)
            {
                return Problem(statusCode: 500, detail: ex.Message);
            }
            catch (Exception)
            {
                return Problem(statusCode: 500);
            }
        }
        [HttpPost("")]
        public async Task<IActionResult> Create([FromBody] ProjectCreateViewModel projectCreate)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var user = await _userManager.FindByIdAsync(projectCreate.UserId);
                    if (user == null)
                        return BadRequest();
                    var client = await _clientService.GetByUserIdAsync(user.Id);
                    if (client == null)
                        return BadRequest();

                    ProjectDTO project = new ProjectDTO { Title = projectCreate.Title, ClientId = client.Id };
                    await _projectService.AddAsync(project);
                    return StatusCode(201);
                }
                catch (TrackValidationException ex)
                {
                    return Problem(statusCode: 500, detail: ex.Message);
                }
                catch (Exception)
                {
                    return Problem(statusCode: 500);
                }
            }
            var problemDetails = new ValidationProblemDetails(ModelState)
            {
                Status = StatusCodes.Status400BadRequest,
            };
            return ValidationProblem(problemDetails);
        }
        [HttpPut("edit")]
        public async Task<IActionResult> Update([FromBody] ProjectDTO projectDTO)
        {
            try
            {
                var project = _projectService.GetAll().FirstOrDefault(x => x.ClientId == projectDTO.ClientId);
                project.ClientId = projectDTO.ClientId;
                project.Title = projectDTO.Title;
                await _projectService.UpdateAsync(project);
                return Ok();
            }
            catch (TrackValidationException ex)
            {
                return Problem(statusCode: 500, detail: ex.Message);
            }
            catch (Exception)
            {
                return Problem(statusCode: 500);
            }
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _projectService.DeleteByIdAsync(id);
                return Ok();
            }
            catch (TrackValidationException ex)
            {
                return Problem(statusCode: 500, detail: ex.Message);
            }
            catch (Exception)
            {
                return Problem(statusCode: 500);
            }
        }
    }
}
