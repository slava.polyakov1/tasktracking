﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using TaskTracking.Api.Models;
using TaskTracking.Api.Models.Employee;
using TaskTracking.Business.Interfaces.Workflow;
using TaskTracking.Business.Models.Workflow;
using TaskTracking.Business.Validation;
using TaskTracking.Data.Entities.Identity;
using TaskTracking.GlobalVariables;

namespace TaskTracking.Api.Controllers.Workflow
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IEmployeeService _employeeService;
        private readonly IProjectService _projectService;
        public EmployeeController(UserManager<ApplicationUser> userManager,
            IEmployeeService employeeService, IProjectService projectService)
        {
            _userManager = userManager;
            _employeeService = employeeService;
            _projectService = projectService;
        }
        [Authorize(Roles = Constants.AdminManager)]
        [HttpGet("all")]
        public IActionResult Get()
        {
            try
            {
                var employees = _employeeService.GetAll();
                return Ok(new { data = employees });
            }
            catch (Exception)
            {
                return Problem(statusCode: 500);
            }
        }
        [Authorize(Roles = Constants.AdminManagerEmployee)]
        [HttpGet("")]
        public async Task<IActionResult> GetEmployee()
        {
            try
            {
                var userClaimId = User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);
                if (userClaimId == null)
                    return BadRequest();

                var user = await _userManager.FindByIdAsync(userClaimId?.Value);
                if (user == null)
                    return Forbid();

                bool isInRole = await _userManager.IsInRoleAsync(user, "Employee");
                if (!isInRole)
                    return BadRequest("User should have a role 'Employee'");

                var employee = await _employeeService.GetByUserIdAsync(user.Id);
                return Ok(new { data = employee });
            }
            catch (Exception)
            {
                return Problem(statusCode: 500);
            }
        }
        [Authorize(Roles = Constants.AdminEmployee)]
        [HttpPost("")]
        public async Task<IActionResult> Create(UserViewModel employeeCreate)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var user = await _userManager.FindByIdAsync(employeeCreate.UserId);
                    if (user == null)
                        return BadRequest();

                    bool isInRole = await _userManager.IsInRoleAsync(user, "Employee");
                    if (isInRole)
                    {
                        EmployeeDTO employee = new EmployeeDTO { UserId = user.Id };
                        await _employeeService.AddAsync(employee);
                        await _userManager.AddToRoleAsync(user, "Employee");
                        return StatusCode(201);
                    }
                    return BadRequest();
                }
                catch (TrackValidationException ex)
                {
                    return BadRequest(ex.Message);
                }
                catch (Exception)
                {
                    return Problem(statusCode: 500);
                }
            }
            var problemDetails = new ValidationProblemDetails(ModelState)
            {
                Status = StatusCodes.Status400BadRequest,
            };
            return ValidationProblem(problemDetails);
        }
        [Authorize(Roles = Constants.AdminManager)]
        [HttpPut("project/{id}")]
        public async Task<IActionResult> AssignToProject(int id, EmployeeViewModel model)
        {
            try
            {
                var project = await _projectService.GetByIdAsync(id);
                if (project == null)
                    return BadRequest();

                var employee = await _employeeService.GetByIdAsync(model.EmployeeId);
                if (employee == null)
                    return BadRequest();

                employee.Projects.Add(project.Id);
                await _employeeService.UpdateAsync(employee);
                return Ok();
            }
            catch (TrackValidationException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception)
            {
                return Problem(statusCode: 500);
            }
        }
        [Authorize(Roles = Constants.AdminManager)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _employeeService.DeleteByIdAsync(id);
                return Ok();
            }
            catch (Exception)
            {
                return Problem(statusCode: 500);
            }
        }
    }
}
