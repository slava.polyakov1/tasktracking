﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using TaskTracking.Api.Models.Client;
using TaskTracking.Api.Models.CLient;
using TaskTracking.Business.Interfaces.Workflow;
using TaskTracking.Business.Models.Workflow;
using TaskTracking.Business.Validation;
using TaskTracking.Data.Entities.Identity;
using TaskTracking.GlobalVariables;

namespace TaskTracking.Api.Controllers.Workflow
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class ClientController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IClientService _clientService;
        public ClientController(UserManager<ApplicationUser> userManager, IClientService clientService)
        {
            _userManager = userManager;
            _clientService = clientService;
        }
        [Authorize(Roles = Constants.AdminManager)]
        [HttpGet("all")]
        public IActionResult Get()
        {
            try
            {
                var clients = _clientService.GetAll();
                return Ok(new { clients });
            }
            catch (Exception)
            {
                return Problem(statusCode: 500);
            }
        }
        [Authorize(Roles = Constants.AdminClientUser)]
        [HttpGet("")]
        public async Task<IActionResult> GetClient()
        {
            try
            {
                var userClaimId = User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);
                if (userClaimId == null)
                    return BadRequest();

                var user = await _userManager.FindByIdAsync(userClaimId?.Value);
                if (user == null)
                    return Forbid();

                bool isInRole = await _userManager.IsInRoleAsync(user, "Client");
                if (!isInRole)
                    return BadRequest("User should have a role 'Client'");

                var client = await _clientService.GetByUserIdAsync(user.Id);
                return Ok(new { data = client });
            }
            catch (Exception)
            {
                return Problem(statusCode: 500);
            }
        }
        [Authorize(Roles = Constants.AdminClient)]
        [HttpPost("")]
        public async Task<IActionResult> CreateClient([FromBody] ClientCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var user = await _userManager.FindByIdAsync(model.UserId);
                    if (user == null)
                        return BadRequest();

                    bool isInRole = await _userManager.IsInRoleAsync(user, "Client");
                    if (!isInRole)
                        return BadRequest("User should have a role 'Client'");

                    ClientDTO client = new ClientDTO { CompanyName = model.CompanyName, UserId = user.Id };
                    await _clientService.AddAsync(client);
                    return StatusCode(201);
                }
                catch (TrackValidationException ex)
                {
                    return BadRequest(ex.Message);
                }
                catch (Exception)
                {
                    return Problem(statusCode: 500);
                }
            }
            var problemDetails = new ValidationProblemDetails(ModelState)
            {
                Status = StatusCodes.Status400BadRequest,
            };
            return ValidationProblem(problemDetails);
        }
        [Authorize(Roles = Constants.AdminClient)]
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateCompanyName(ClientCompanyNameViewModel model, int id)
        {
            try
            {
                await _clientService.UpdateCompanyNameAsync(model.CompanyName, id);
                return Ok();
            }
            catch (TrackValidationException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception)
            {
                return Problem(statusCode: 500);
            }
        }
        [Authorize(Roles = Constants.AdminRole)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _clientService.DeleteByIdAsync(id);
                return Ok();
            }
            catch (Exception)
            {
                return Problem(statusCode: 500);
            }
        }
    }
}
