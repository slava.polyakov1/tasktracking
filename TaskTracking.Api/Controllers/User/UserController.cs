﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using TaskTracking.Data.Entities.Identity;

namespace TaskTracking.Api.Controllers.User
{
    [Produces("application/json")]
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        public UserController(UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }
        [HttpGet("")]
        public async Task<IActionResult> GetUser()
        {
            try
            {
                var userClaimId = User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);
                if (userClaimId == null)
                    return BadRequest();

                var user = await _userManager.FindByIdAsync(userClaimId?.Value);
                if (user == null)
                    return Forbid();

                return Ok(new { data = user });
            }
            catch (Exception)
            {
                return Problem(statusCode: 500);
            }
        }
        [HttpGet("all")]
        public IActionResult GetUsers()
        {
            try
            {
                var users = _userManager.Users;
                return Ok(new { data = users });
            }
            catch (Exception)
            {
                return Problem(statusCode: 500);
            }
        }
        [HttpGet("roles/all")]
        public IActionResult GetRoles()
        {
            try
            {
                var roles = _roleManager.Roles;
                return Ok(new { roles = roles.ToList() });
            }
            catch (Exception)
            {
                return Problem(statusCode: 500);
            }
        }
        [HttpGet("roles")]
        public async Task<IActionResult> GetUserRoles()
        {
            try
            {
                var userClaimId = User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);
                if (userClaimId == null)
                    return BadRequest();

                var user = await _userManager.FindByIdAsync(userClaimId?.Value);
                if (user == null)
                    return Forbid();

                var roles = await _userManager.GetRolesAsync(user);
                return Ok(new { roles = roles.ToList() });
            }
            catch (Exception)
            {
                return Problem(statusCode: 500);
            }
        }
    }
}
