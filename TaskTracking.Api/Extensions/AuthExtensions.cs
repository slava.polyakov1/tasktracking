﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using TaskTracking.Business.Settings;

namespace TaskTracking.Api.Extensions
{
    public static class AuthExtensions
    {
        /// <summary>
        /// Jwt bearer extension
        /// </summary>
        /// <param name="services"></param>
        /// <param name="jwtSettings"></param>
        /// <returns></returns>
        public static IServiceCollection AddAuth(
            this IServiceCollection services,
            JwtSettings jwtSettings)
        {
            services
                .AddAuthentication(options =>
                {
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultForbidScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(options =>
                {
                    options.SaveToken = true;
                    options.RequireHttpsMetadata = true;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidIssuer = jwtSettings.Issuer,
                        ValidAudience = jwtSettings.Issuer,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(jwtSettings.Secret)),
                        ValidateIssuer = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                    };
                    //options.SecurityTokenValidators.Clear();
                    //options.SecurityTokenValidators.Add(new JwtSecurityTokenHandler
                    //{
                    //    InboundClaimTypeMap = new Dictionary<string, string>()
                    //});
                });

            return services;
        }
    }
}
